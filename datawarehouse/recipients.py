"""Logic for recipient rule parsing."""
from django.db.models import Exists
from django.db.models import OuterRef
from django.db.models import Q

from datawarehouse import models


def get_failed_tests(checkout, test_regex=None):
    """Return a set of all failed tests with id matching a regex."""
    kcidbtest_issueoccurrences = models.IssueOccurrence.objects.filter(
        kcidb_test=OuterRef('iid')
    ).values('id')

    unresolved_or_regression = kcidbtest_issueoccurrences.filter(
        Q(issue__resolved_at__isnull=False) | Q(related_checkout=checkout, is_regression=True)
    ).values('id')

    query = models.KCIDBTest.objects.filter(
        build__checkout=checkout,
        status=models.ResultEnum.FAIL,
        waived=False
    ).filter(
        ~Exists(kcidbtest_issueoccurrences) | Exists(unresolved_or_regression)
    )

    if test_regex is not None:
        query = query.filter(id__regex=test_regex)

    return query.values('test')


def get_failed_subtests(checkout, subtest_regex=None):
    """Return a set of all failed subtests with id matching a regex."""
    query = models.KCIDBTestResult.objects.filter(
        test__build__checkout=checkout,
        status=models.ResultEnum.FAIL,
    )
    # I suppose we might want another filter here once subtest triaging is required
    # https://gitlab.com/groups/cki-project/-/epics/115 similar to get_failed_tests:
    #    ~Exists(kcidbtestresult_issueoccurrences) | Exists(unresolved_or_regression)

    if subtest_regex is not None:
        query = query.filter(id__regex=subtest_regex)

    return query.values('id')


def when_always(*_):
    """Evaluate condition for when: always."""
    return True


def when_failed(checkout, *_):
    """Evaluate condition for when: failed."""
    return not checkout.succeeded


def failed_tests_maintainers(checkout):
    """Return list of maintainers for failed tests."""
    tests = get_failed_tests(checkout, None)

    emails = models.TestMaintainer.objects.filter(test__in=tests).exclude(email=None)\
                                  .values_list("email", flat=True)

    return list(emails)


def when_failed_tests(checkout, test_regex=None):
    """Evaluate condition for when: failed_tests."""
    if test_regex is None:
        return checkout.has_untriaged_failed_tests

    return get_failed_tests(checkout, test_regex)


def when_failed_subtests(checkout, subtest_regex):
    """Evaluate condition for when: failed_subtests."""
    return get_failed_subtests(checkout, subtest_regex)


def submitter(checkout):
    """
    Return submitter email.

    Returned value is a list to match other parsers output.
    """
    if not checkout.submitter:
        return []
    return [checkout.submitter.email]


class Recipients:
    # pylint: disable=too-few-public-methods
    """Email recipients information."""

    evaluation_rules = {
        'always': when_always,
        'failed': when_failed,
        'failed_tests': when_failed_tests,
        'failed_subtests': when_failed_subtests,
    }

    recipients_keywords = {
        'failed_tests_maintainers': failed_tests_maintainers,
        'submitter': submitter,
    }

    def __init__(self, checkout):
        """Init."""
        self.checkout = checkout

    def _parse_recipients(self, recipients):
        """Parse list of recipients."""
        recipients = set(recipients) if isinstance(recipients, list) else {recipients}

        for key, key_parser in self.recipients_keywords.items():
            if key in recipients:
                recipients.remove(key)
                recipients.update(key_parser(self.checkout))

        return list(recipients)

    def _conditions_match(self, rule):
        """Evaluate rule conditions."""
        return self.evaluation_rules[rule['when']](self.checkout, rule.get('which'))

    def render(self):
        """Parse reporting rules and return recipients."""
        targets = ['To', 'BCC']
        recipients = {}

        for rule in self.checkout.report_rules:
            if not self._conditions_match(rule):
                continue
            for target in targets:
                if field := rule.get(f'send_{target.lower()}'):
                    for recipient in self._parse_recipients(field):
                        recipients[recipient] = (target, rule.get('report_template', 'default'))
            if rule.get('override'):
                break

        grouped_recipients = {}
        for recipient, (target, template) in recipients.items():
            if template not in grouped_recipients:
                grouped_recipients[template] = {}
            if target not in grouped_recipients[template]:
                grouped_recipients[template][target] = set()
            grouped_recipients[template][target].add(recipient)

        return grouped_recipients
