"""Timetag."""
import datetime

from django import template

register = template.Library()


def timetag(timestamp):
    """Convert seconds into HH:MM:SS."""
    try:
        ts = float(timestamp)
    except (ValueError, TypeError):
        return '-'

    time = datetime.datetime.fromtimestamp(ts)
    return time.strftime('%H:%M:%S')


register.filter(timetag)
