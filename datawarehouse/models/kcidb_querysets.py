"""Custom QuerySets for KCIDB models."""
from django.db import models

from datawarehouse.models import issue_models
from datawarehouse.models import kcidb_models
from datawarehouse.models import pipeline_models
from datawarehouse.models import test_models
from datawarehouse.models.utils import AuthorizedQuerySet


class KCIDBCheckoutQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBCheckout."""

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        checkout = kcidb_models.KCIDBCheckout.objects.filter(iid=models.OuterRef('iid'))
        checkout_untriaged = checkout.filter(
            valid=False,
            issues=None
        )

        builds_run = kcidb_models.KCIDBBuild.objects.filter(checkout__iid=models.OuterRef('iid'))
        builds_running = builds_run.filter(valid=None)
        builds_pass = builds_run.filter(valid=True)
        builds_fail = builds_run.filter(valid=False)
        builds_untriaged = builds_fail.filter_untriaged()

        tests_run = kcidb_models.KCIDBTest.objects.filter(build__checkout__iid=models.OuterRef('iid'))
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL)
        tests_miss = tests_run.filter(status=test_models.ResultEnum.MISS)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)
        tests_untriaged = tests_run.filter_untriaged()

        tests_actionable = tests_untriaged.exclude(waived=True)
        tests_triaged = tests_run.filter_triaged()
        tests_act_fail = tests_actionable.filter(status=test_models.ResultEnum.FAIL)
        tests_act_miss = tests_actionable.filter(status=test_models.ResultEnum.MISS)
        tests_act_error = tests_actionable.filter(status=test_models.ResultEnum.ERROR)

        counts = {
            'builds_run': builds_run,
            'builds_running': builds_running,
            'builds_pass': builds_pass,
            'builds_fail': builds_fail,
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_fail': tests_fail,
            'tests_miss': tests_miss,
            'tests_error': tests_error,
            'tests_act_fail': tests_act_fail,
            'tests_act_miss': tests_act_miss,
            'tests_act_error': tests_act_error,
            'tests_triaged': tests_triaged,
        }

        # Look for issues to determine if there are objects triaged.
        checkout_issues = issue_models.Issue.objects.filter(
            kcidbcheckout__iid=models.OuterRef('iid')
        )
        build_issues = issue_models.Issue.objects.filter(
            kcidbbuild__checkout__iid=models.OuterRef('iid')
        )
        test_issues = issue_models.Issue.objects.filter(
            kcidbtest__build__checkout__iid=models.OuterRef('iid')
        )

        annotations = {
            # Child objects passed
            'stats_builds_passed': ~models.Exists(builds_fail),

            # Objects have issues
            'stats_checkout_triaged': models.Exists(checkout_issues),
            'stats_builds_triaged': models.Exists(build_issues),
            'stats_tests_triaged': models.Exists(test_issues),

            # Objects have failures without issues
            'stats_checkout_untriaged': models.Exists(checkout_untriaged),
            'stats_builds_untriaged': models.Exists(builds_untriaged),
            'stats_tests_untriaged': models.Exists(tests_untriaged),

            # Overall checkout tests status
            'stats_tests_actionable': models.Exists(tests_actionable),
            'stats_tests_status': models.Case(
                models.When(models.Exists(tests_running), then=models.Value(None)),
                models.When(models.Exists(tests_act_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_act_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_act_miss), then=models.Value(test_models.ResultEnum.MISS)),
                models.When(models.Exists(tests_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_miss), then=models.Value(test_models.ResultEnum.MISS)),
                models.When(models.Exists(tests_pass), then=models.Value(test_models.ResultEnum.PASS)),
                models.When(models.Exists(tests_skip), then=models.Value(test_models.ResultEnum.SKIP)),
                default=None,
                output_field=models.CharField(max_length=1, choices=test_models.ResultEnum.choices)
            ),

            # Overall checkout build status
            'stats_builds_actionable': models.Exists(builds_untriaged),
            'stats_builds_status': models.Case(
                models.When(models.Exists(builds_running), then=models.Value(None)),
                models.When(models.Exists(builds_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(builds_pass), then=models.Value(test_models.ResultEnum.PASS)),
                default=None,
                output_field=models.CharField(max_length=1, choices=test_models.ResultEnum.choices)
            )
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('iid').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def annotated_by_architecture(self):
        # pylint: disable=too-many-locals
        """Add build and test information by architecture."""
        counts = {}
        for arch in pipeline_models.ArchitectureEnum:
            builds_run = (
                kcidb_models.KCIDBBuild.objects
                .filter(checkout__iid=models.OuterRef('iid'), architecture=arch)
                .exclude(valid=None)
            )
            builds_fail = builds_run.filter(valid=False)
            tests_run = (
                kcidb_models.KCIDBTest.objects
                .filter(build__checkout__iid=models.OuterRef('iid'), build__architecture=arch)
                .exclude(status=None)
            )
            tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL)

            counts.update({
                f'{arch.name}_builds_ran': builds_run,
                f'{arch.name}_builds_failed': builds_fail,
                f'{arch.name}_builds_failed_untriaged': builds_fail.filter_untriaged(),
                f'{arch.name}_builds_with_issues': builds_fail.exclude(issues=None),
                f'{arch.name}_tests_ran': tests_run,
                f'{arch.name}_tests_failed': tests_fail.exclude(waived=True),
                f'{arch.name}_tests_failed_waived': tests_fail.filter(waived=True),
                f'{arch.name}_tests_failed_untriaged': tests_fail.filter_untriaged().exclude(waived=True),
                f'{arch.name}_tests_with_issues': tests_run.exclude(issues=None),
            })

        annotations = {}
        for name, query in counts.items():
            count = query.values('iid').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_ready_to_report(self):
        """
        Get checkouts ready to report.

        To match the 'ready_to_report' condition, a checkout must match
        all the following conditions:

        - Not be already tagged ready_to_report (ready_to_report == False).
        - Checkout valid != None (finished) and last_triaged_at != None (triaged).
        - If Builds exist, all Builds valid != None (finished) and last_triaged_at != None (triaged).
        - If Test exist, all Tests status != None (finished) and last_triaged_at != None (triaged).
        """
        annotations = {
            'unfinished_checkout': models.Exists(kcidb_models.KCIDBCheckout.objects.filter(
                models.Q(iid=models.OuterRef('iid')) & (
                    models.Q(valid=None) |
                    models.Q(last_triaged_at=None)
                )
            )),
            'unfinished_builds': models.Exists(kcidb_models.KCIDBBuild.objects.filter(
                models.Q(checkout__iid=models.OuterRef('iid')) & (
                    models.Q(valid=None) |
                    models.Q(last_triaged_at=None)
                )
            )),
            'missing_testplan': (
                # There are not KCIDBBuild with test_plan_missing, as long as there are no failed builds.
                models.ExpressionWrapper(
                    models.Exists(kcidb_models.KCIDBBuild.objects.filter(
                        models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(test_plan_missing=True)
                    )) & ~ models.Exists(kcidb_models.KCIDBBuild.objects.filter(
                        models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(valid=False)
                    )),
                    output_field=models.BooleanField()
                )
            ),
            'unfinished_tests': models.Exists(kcidb_models.KCIDBTest.objects.filter(
                models.Q(build__checkout__iid=models.OuterRef('iid')) & (
                    models.Q(status=None) |
                    models.Q(last_triaged_at=None)
                )
            )),
        }

        return (
            self
            .filter(ready_to_report=False)
            .annotate(**annotations)
            .exclude(
                models.Q(unfinished_checkout=True) |
                models.Q(unfinished_builds=True) |
                models.Q(missing_testplan=True) |
                models.Q(unfinished_tests=True)
            )
        )

    def filter_build_setups_finished(self):
        """Get checkouts with all the build setups finished.

        To achieve this match, we're checking the following conditions:
        - notification_sent_build_setups_finished must be False
        - Checkout must have any Build, and all Builds must have kpet_tree_name != None
        """
        annotations = {
            'unfinished_build_setups': models.Exists(kcidb_models.KCIDBBuild.objects.filter(
                (models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(kpet_tree_name=None))
            )
            ),
            'has_builds': models.Exists(kcidb_models.KCIDBBuild.objects.filter(
                models.Q(checkout__iid=models.OuterRef('iid'))
            )
            ),
        }

        return (
            self
            .filter(notification_sent_build_setups_finished=False)
            .annotate(**annotations)
            .exclude(models.Q(has_builds=False))
            .exclude(models.Q(unfinished_build_setups=True))
        )

    def filter_tests_finished(self):
        """Get checkout with all tests finished.

        To achieve this match, we're checking the following conditions:
        - notification_sent_build_setups_finished must be False
        - Checkout must have any Test, and all Tests status != None (finished).
        - Checkout must not have Builds with test_plan_missing.
        """
        annotations = {
            'unfinished_tests': models.Exists(kcidb_models.KCIDBTest.objects.filter(
                models.Q(build__checkout__iid=models.OuterRef('iid')) & models.Q(status=None)
            )
            ),
            'has_tests': models.Exists(kcidb_models.KCIDBTest.objects.filter(
                models.Q(build__checkout__iid=models.OuterRef('iid'))
            )
            ),
            'has_builds_with_test_plan_missing': models.Exists(kcidb_models.KCIDBBuild.objects.filter(
                models.Q(checkout__iid=models.OuterRef('iid')) & models.Q(test_plan_missing=True)
            )
            ),
        }

        return (
            self
            .filter(notification_sent_tests_finished=False)
            .annotate(**annotations)
            .exclude(models.Q(has_tests=False))
            .exclude(models.Q(unfinished_tests=True))
            .exclude(models.Q(has_builds_with_test_plan_missing=True))
        )

    def filter_has_builds(self):
        """Get checkout with any build."""
        return self.filter(
            models.Exists(
                kcidb_models.KCIDBBuild.objects.filter(models.Q(checkout__iid=models.OuterRef('iid')))
            )
        )

    def filter_has_tests(self):
        """Get checkout with any test."""
        return self.filter(
            models.Exists(
                kcidb_models.KCIDBTest.objects.filter(build__checkout__iid=models.OuterRef('iid'))
            )
        )

    def filter_untriaged(self):
        """Filter untriaged checkouts."""
        issueoccurrences = issue_models.IssueOccurrence.objects.filter(kcidb_checkout_id=models.OuterRef("iid"))
        untriaged = models.Q(~models.Exists(issueoccurrences))

        return self.filter(
            untriaged,
            valid=False,
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBBuildQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBBuild."""

    def aggregated(self):
        # pylint: disable=too-many-locals
        """Add aggregated information."""
        tests_run = kcidb_models.KCIDBTest.objects.filter(build__id=models.OuterRef('id'))
        tests_running = tests_run.filter(status=None)
        tests_pass = tests_run.filter(status=test_models.ResultEnum.PASS)
        tests_skip = tests_run.filter(status=test_models.ResultEnum.SKIP)
        tests_miss = tests_run.filter(status=test_models.ResultEnum.MISS)
        tests_fail = tests_run.filter(status=test_models.ResultEnum.FAIL)
        tests_error = tests_run.filter(status=test_models.ResultEnum.ERROR)

        tests_untriaged = tests_run.filter_untriaged()

        tests_actionable = tests_untriaged.exclude(waived=True)
        tests_triaged = tests_run.filter_triaged()
        tests_act_fail = tests_actionable.filter(status=test_models.ResultEnum.FAIL)
        tests_act_miss = tests_actionable.filter(status=test_models.ResultEnum.MISS)
        tests_act_error = tests_actionable.filter(status=test_models.ResultEnum.ERROR)

        counts = {
            'tests_run': tests_run,
            'tests_running': tests_running,
            'tests_pass': tests_pass,
            'tests_skip': tests_skip,
            'tests_miss': tests_miss,
            'tests_fail': tests_fail,
            'tests_error': tests_error,
            'tests_act_fail': tests_act_fail,
            'tests_act_miss': tests_act_miss,
            'tests_act_error': tests_act_error,
            'tests_triaged': tests_triaged,
        }

        annotations = {
            'stats_tests_untriaged': models.Exists(tests_untriaged),

            # Overall checkout tests status
            'stats_tests_actionable': models.Exists(tests_actionable),
            'stats_tests_status': models.Case(
                models.When(models.Exists(tests_running), then=models.Value(None)),
                models.When(models.Exists(tests_act_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_act_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_act_miss), then=models.Value(test_models.ResultEnum.MISS)),
                models.When(models.Exists(tests_fail), then=models.Value(test_models.ResultEnum.FAIL)),
                models.When(models.Exists(tests_error), then=models.Value(test_models.ResultEnum.ERROR)),
                models.When(models.Exists(tests_miss), then=models.Value(test_models.ResultEnum.MISS)),
                models.When(models.Exists(tests_pass), then=models.Value(test_models.ResultEnum.PASS)),
                models.When(models.Exists(tests_skip), then=models.Value(test_models.ResultEnum.SKIP)),
                default=None,
                output_field=models.CharField(max_length=1, choices=test_models.ResultEnum.choices)
            )
        }

        # Add count element for all the queries listed on $counts.
        for name, query in counts.items():
            count = query.values('id').order_by().annotate(c=models.Count('*')).values('c')
            count.query.set_group_by()
            annotations[f'stats_{name}_count'] = models.Subquery(count, output_field=models.IntegerField())

        return self.annotate(**annotations)

    def filter_untriaged(self):
        """Filter untriaged builds."""
        issueoccurrences = issue_models.IssueOccurrence.objects.filter(kcidb_build_id=models.OuterRef("iid"))
        untriaged = models.Q(~models.Exists(issueoccurrences))

        return self.filter(
            untriaged,
            valid=False,
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBTestQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBTestQuerySet."""

    def annotate_is_untriaged(self):
        """Annotate queryset with the boolean `is_untriaged` field.

        If the test has no subtests, it will be True when there're issues linked to the test,
        otherwise it will be True if all subtests are linked to a test.
        """
        # Only unsuccessful things that are not waived can be triaged
        unsuccessful_status = models.Q(status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES)
        not_waived = ~models.Q(waived=True)

        issueoccurrences = issue_models.IssueOccurrence.objects.filter(kcidb_test_id=models.OuterRef("iid"))
        is_untriaged = models.Q(~models.Exists(issueoccurrences))

        return self.annotate(is_untriaged=unsuccessful_status & not_waived & is_untriaged)

    def filter_triaged(self):
        """Filter triaged tests."""
        return self.annotate_is_untriaged().filter(
            is_untriaged=False,
            status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        )

    def filter_untriaged(self):
        """Filter untriaged tests."""
        return self.annotate_is_untriaged().filter(
            is_untriaged=True,
            status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        )

    def filter_untriaged_blocking(self):
        """Filter untriaged blocking tests."""
        return self.filter_untriaged().filter(
            status=test_models.ResultEnum.FAIL,
        )

    def filter_untriaged_non_blocking(self):
        """Filter untriaged non_blocking tests."""
        # exclude blocking
        return self.filter_untriaged().exclude(
            status=test_models.ResultEnum.FAIL,
        )

    def get_by_natural_key(self, obj_id):
        """Lookup the object by the natural key."""
        return self.get(id=obj_id)


class KCIDBTestResultQuerySet(AuthorizedQuerySet):
    """QuerySet for KCIDBTestResult."""

    def filter_untriaged(self):
        """Filter untriaged test results."""
        issueoccurrences = issue_models.IssueOccurrence.objects.filter(kcidb_testresult_id=models.OuterRef("iid"))
        untriaged = models.Q(~models.Exists(issueoccurrences))

        return self.filter(
            untriaged,
            status__in=kcidb_models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        )
