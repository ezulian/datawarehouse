# pylint: disable=too-few-public-methods
"""Model utils."""

from django.contrib.auth import get_user_model
from django.db import models

from datawarehouse import authorization

User = get_user_model()


class AuthorizedQuerySet(models.QuerySet):
    """QuerySet with authorization methods."""

    def filter_authorized(self, request):
        """Return authorized objects for the request."""
        return authorization.PolicyAuthorizationBackend.filter_authorized(
            request, self
        )

    def filter_read_authorized(self, request):
        """Return read-authorized objects for the request."""
        return authorization.PolicyAuthorizationBackend.filter_read_authorized(
            request, self
        )

    def filter_write_authorized(self, request):
        """Return write-authorized objects for the request."""
        return authorization.PolicyAuthorizationBackend.filter_write_authorized(
            request, self
        )


class GenericNameQuerySet(AuthorizedQuerySet):
    """Natural key based on name."""

    def get_by_natural_key(self, name):
        """Lookup the object by the natural key."""
        return self.get(name=name)


class GenericDescriptionQuerySet(AuthorizedQuerySet):
    """Natural key based on description."""

    def get_by_natural_key(self, description):
        """Lookup the object by the natural key."""
        return self.get(description=description)


class Model(models.Model):
    """Django Model with authorization methods."""

    class Meta:
        """Meta."""

        abstract = True

    @property
    def users_write_authorized(self):
        """Return list of users authorized to write this object."""
        return authorization.PolicyAuthorizationBackend.get_users_authorized(self, 'write')

    @property
    def users_read_authorized(self):
        """Return list of users authorized to read this object."""
        return authorization.PolicyAuthorizationBackend.get_users_authorized(self, 'read')


class CreatedStampedModel(models.Model):
    """Store timestaps and user for objects creation."""

    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='%(class)s_created',
        null=True
    )

    class Meta:
        """Meta."""

        abstract = True


class EditedStampedModel(models.Model):
    """Store timestaps and user for objects modification."""

    last_edited_at = models.DateTimeField(auto_now=True)
    last_edited_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        related_name='%(class)s_edited',
        null=True
    )

    class Meta:
        """Meta."""

        abstract = True


class StampedModel(CreatedStampedModel, EditedStampedModel):
    """Store timestamps and users for objects creation and modification."""

    class Meta:
        """Meta."""

        abstract = True

    @property
    def was_edited(self):
        """
        Return True if the object was modified.

        Considers a small time delta between creation and modification to
        avoid false possitives.
        """
        try:
            return (
                self.last_edited_by is not None and
                (self.last_edited_at - self.created_at).total_seconds() > 1
            )
        except TypeError:
            # Old objects don't have created_by data.
            return (
                self.created_by != self.last_edited_by
            )
