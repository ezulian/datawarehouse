"""Models for scheduled tasks."""
import datetime

from cki_lib.logger import get_logger
from django.db import models
from django.db import transaction
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import celery
from datawarehouse.models.utils import AuthorizedQuerySet

LOGGER = get_logger(__name__)


class QueuedTaskQuerySet(AuthorizedQuerySet):
    """QuerySet for QueuedTask."""

    def filter_ready_to_run(self):
        """Filter QueuedTask ready to run."""
        return self.filter(run_at__lte=timezone.now(), start_time=None)


class QueuedTask(EMOM('queued_task'), models.Model):
    """Model for QueuedTask."""

    # Name matching celery task name
    name = models.CharField(max_length=500)
    # Arbitrary ID to identify unique calls
    call_id = models.CharField(max_length=200)
    # List of kwargs. Each time the task is called, the call kwargs are appended to the list.
    calls_kwargs = models.JSONField(default=list)
    run_at = models.DateTimeField()
    # Timestamp for the moment the task started running.
    # NOTE: Completed tasks are deleted, so durations over one minute signalize issues.
    start_time = models.DateTimeField(null=True, blank=True)

    objects = QueuedTaskQuerySet.as_manager()

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} [{self.call_id}]'

    @classmethod
    def create(cls, name, call_id, call_kwargs, *,
               run_in_minutes=5):
        """Create task or append call_kwargs to an existent task that haven't started yet."""
        calls_kwargs = call_kwargs if isinstance(call_kwargs, list) else [call_kwargs]
        run_at = timezone.now() + datetime.timedelta(minutes=run_in_minutes)

        with transaction.atomic():
            task, created = cls.objects.select_for_update().get_or_create(
                name=name,
                call_id=call_id,
                start_time=None,
                defaults={
                    'calls_kwargs': calls_kwargs,
                    'run_at': run_at,
                }
            )

            if not created:
                task.run_at = max(run_at, task.run_at)
                task.calls_kwargs.extend(calls_kwargs)
                task.save(update_fields=['run_at', 'calls_kwargs'])

        LOGGER.info("QueuedTask: %s %s. Next run: %s",
                    task, 'Created' if created else 'Postponed', task.run_at)

        return task

    def run(self):
        """
        Run the scheduled task.

        The celery task is called with all the calls_kwargs stored for the
        task.
        """
        with transaction.atomic():
            # quickly mark the task as started and release the lock
            updated = (
                QueuedTask.objects.filter_ready_to_run().filter(pk=self.pk)
                .select_for_update(skip_locked=True)
                .update(start_time=timezone.now())
            )
            if updated:
                LOGGER.debug("QueuedTask (%s) selected to run.", self)
            else:
                LOGGER.error("Tried to run QueuedTask (%s), but it's not ready to run or already running", self)
                return

        LOGGER.info("QueuedTask: Running %s", self)
        try:
            celery.app.signature(self.name)(self.calls_kwargs)
        except Exception:  # pylint: disable=broad-except
            LOGGER.exception('Error running QueuedTask (%s), will be retried on a new task.', self)
            # retry as a new task or append, which could mean appending calls_kwargs to an existent scheduled task
            QueuedTask.create(
                name=self.name,
                call_id=self.call_id,
                call_kwargs=self.calls_kwargs,
                run_in_minutes=1,
            )

        self.delete()
        LOGGER.info("QueuedTask (%s) purged.", self)
