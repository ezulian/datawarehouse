# -*- coding: utf-8 -*-
# Copyright (c) 2018 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Misc scripts file."""
from celery import shared_task
from cki_lib.logger import get_logger
from django.conf import settings
from django.utils import timezone

from datawarehouse import models
from datawarehouse import pagination
from datawarehouse import signals

LOGGER = get_logger(__name__)


@shared_task
def send_kcidb_object_for_retriage(calls_kwargs):
    """Add objects to the queue for triaging considering settings.RETRIAGE_DAYS.

    Args:
        calls_kwargs: list of dicts with "issueregex_id"
    """
    date_from = timezone.now() - timezone.timedelta(days=settings.RETRIAGE_DAYS)

    issueregex_ids = {
        call['issueregex_id'] for call in calls_kwargs
        # Make sure the regex still exists (was not deleted right after it was created)
        if models.IssueRegex.objects.filter(id=call['issueregex_id']).exists()
    }
    if not issueregex_ids:
        return

    checkouts = models.KCIDBCheckout.objects.filter(start_time__gte=date_from)
    to_retriage = {
        'checkout': checkouts.filter(
            valid=False,
        ).order_by('-iid'),
        'build': models.KCIDBBuild.objects.filter(
            checkout__in=checkouts,
            valid=False,
        ).order_by('-iid'),
        'test': models.KCIDBTest.objects.filter(
            build__checkout__in=checkouts,
            status__in=models.KCIDBTest.UNSUCCESSFUL_STATUSES,
        ).order_by('-iid'),
    }

    for obj_type, objects in to_retriage.items():
        total = objects.count()
        paginator = pagination.EndlessPaginator(objects, settings.RETRIAGE_PAGE_SIZE)
        page_number = 1
        while page := paginator.get_page(page_number):
            LOGGER.info("Sending %s to retriage: %d/%d items",
                        obj_type, min(page_number * settings.RETRIAGE_PAGE_SIZE, total), total)
            signals.kcidb_object.send(
                sender='scripts.misc.send_kcidb_object_for_retriage',
                status=models.ObjectStatusEnum.NEEDS_TRIAGE,
                object_type=obj_type,
                objects=page,
                misc={'issueregex_ids': list(issueregex_ids)},
            )
            page_number += 1
