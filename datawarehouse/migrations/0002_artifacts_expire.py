# Generated by Django 3.1.1 on 2020-09-09 15:07

import datetime
from django.utils import timezone
from django.db import migrations, models


def populate_expiry_date(apps, schema_editor):
    """Populate expiry_date with default values."""
    Artifact = apps.get_model('datawarehouse', 'Artifact')
    db_alias = schema_editor.connection.alias

    expiry_date = timezone.now() + datetime.timedelta(days=60)
    Artifact.objects.using(db_alias).update(expiry_date=expiry_date)


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='artifact',
            name='expiry_date',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='artifact',
            name='valid_for',
            field=models.PositiveSmallIntegerField(default=60),
        ),
        migrations.RunPython(populate_expiry_date),
    ]
