"""Populate IssueOccurrence model."""
from django.db import migrations, models
from django.db.models import Count


def migrate_to_new_model(apps, schema_editor):
    """Migrate Issue Occurrences to new IssueOccurrence model."""
    KCIDBCheckout = apps.get_model('datawarehouse', 'KCIDBCheckout')
    KCIDBBuild = apps.get_model('datawarehouse', 'KCIDBBuild')
    KCIDBTest = apps.get_model('datawarehouse', 'KCIDBTest')
    db_alias = schema_editor.connection.alias

    checkouts = KCIDBCheckout.objects.using(db_alias).exclude(issues=None).prefetch_related('issues')
    builds = KCIDBBuild.objects.using(db_alias).exclude(issues=None).prefetch_related('issues')
    tests = KCIDBTest.objects.using(db_alias).exclude(issues=None).prefetch_related('issues')

    for checkout in checkouts:
        checkout.issues_2.set(checkout.issues.all())

    for build in builds:
        build.issues_2.set(build.issues.all())

    for test in tests:
        test.issues_2.set(test.issues.all())


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0045_issue_ocurrence_intermediate'),
    ]

    operations = [
        migrations.RunPython(migrate_to_new_model),
    ]
