# Generated by Django 3.2.13 on 2022-07-04 10:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0105_alter_kcidbbuild_config_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pipeline',
            name='gittree',
        ),
        migrations.RemoveField(
            model_name='pipeline',
            name='kcidb_checkout',
        ),
        migrations.RemoveField(
            model_name='pipeline',
            name='project',
        ),
        migrations.RemoveField(
            model_name='triggervariable',
            name='pipeline',
        ),
        migrations.RemoveField(
            model_name='patch',
            name='pipelines',
        ),
        migrations.DeleteModel(
            name='GitlabJob',
        ),
        migrations.DeleteModel(
            name='Pipeline',
        ),
        migrations.DeleteModel(
            name='Project',
        ),
        migrations.DeleteModel(
            name='TriggerVariable',
        ),
    ]
