"""Email sending tooling."""
from cki_lib.logger import get_logger
from django.conf import settings
from django.core.mail import EmailMessage
from django.db import transaction
import prometheus_client

from datawarehouse import models

LOGGER = get_logger(__name__)

EMAILS_SENT = prometheus_client.Counter(
    'cki_dw_email_messages_sent',
    'Count of sent emails',
)


class EmailQueue:
    """Email sending Queue."""

    @staticmethod
    def send():
        """Send messages in the queue."""
        messages = (
            models.MessagePending.objects
            .select_for_update(skip_locked=True)
            .filter(kind=models.MessageKindEnum.EMAIL)
        )
        with transaction.atomic():
            for message in messages:
                LOGGER.info("Sending email: subject=%s recipients=%s",
                            message.content['subject'],
                            message.content['recipients'])
                EmailMessage(
                    subject=message.content['subject'],
                    body=message.content['body'],
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=message.content['recipients'].get('to', []),
                    cc=message.content['recipients'].get('cc', []),
                    bcc=message.content['recipients'].get('bcc', []),
                    headers=message.content['headers'],
                ).send()
                EMAILS_SENT.inc()
                message.delete()

    @staticmethod
    def add(subject, body, recipients, *, headers=None):
        """
        Add message to the outgoing queue.

        Arguments:
            - subject: (str) Subject of the message.
            - body: (str) Content of the message.
            - recipients: (str|list|dict) Recipients for the message. If string or list is provided
              the addresses are treated as 'to'. If dict, 'to', 'cc' and 'bcc' keys are allowed
              containing a list of recipients for each field.
            - headers: (dict) Custom headers to include on the message.
        """
        if isinstance(recipients, str):
            recipients = {'to': [recipients]}
        elif isinstance(recipients, list):
            recipients = {'to': recipients}

        models.MessagePending.objects.create(
            kind=models.MessageKindEnum.EMAIL,
            content={
                'subject': subject,
                'body': body,
                'recipients': recipients,
                'headers': headers or {},
            }
        )
