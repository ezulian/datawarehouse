# pylint: disable=too-many-lines
"""Test model creation from KCIDB data."""
import datetime
import json
import os
import pathlib
from unittest import mock

import responses

from datawarehouse import models
from tests import utils


def load_json(name):
    """Load json file from assets."""
    file_name = os.path.join(utils.ASSETS_DIR, name)
    file_content = pathlib.Path(file_name).read_text(encoding='utf-8')

    return json.loads(file_content)


def mock_patch():
    """Mock patch requests."""
    patch_body = """MIME-Version: 1.0
Subject: [RHEL PATCH 206/206] fix some stuff, and break some other
commit 56887cffe946bb0a90c74429fa94d6110a73119d
Author: Patch Author <patch@author.com>
Date:   Mon Feb 22 10:48:09 2021 +0100

    fix some stuff, and break some other
    """
    responses.add(responses.GET, 'http://patchwork.server/patch/2322797/mbox/', body=patch_body)


class TestMaintainer(utils.TestCase):
    """Tests for the kcidb_models.Maintainer."""

    def test__str__(self):
        """Test Maintainer.__str__."""
        maintainer = models.Maintainer(name='Someone', email='someone@mail.com')

        self.assertEqual(str(maintainer), 'Someone <someone@mail.com>')

    def test_create_from_address(self):
        """Test Maintainer.create_from_address works as expected."""
        maintainer = models.Maintainer.create_from_address('Someone <someone@mail.com>')

        self.assertEqual(maintainer.email, 'someone@mail.com')
        self.assertEqual(maintainer.name, 'Someone')


class TestKCIDBOrigin(utils.TestCase):
    """Tests for the kcidb_models.KCIDBOrigin."""

    def test__str__(self):
        """Test KCIDBOrigin.__str__."""
        origin = models.KCIDBOrigin(name='original')

        self.assertEqual(str(origin), 'original')

    def test_create_from_address(self):
        """Test KCIDBOrigin.create_from_address works as expected."""
        origin = models.KCIDBOrigin.create_from_string('original')

        self.assertEqual(origin.name, 'original')


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBCheckoutFromJson(utils.TestCase):
    """Test creation of KCIDBCheckout model."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    def test_basic(self):
        """Submit only id."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsInstance(checkout, models.KCIDBCheckout)

    def test_re_submit(self):
        """Submit twice. Updates current data."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNone(checkout.message_id)

        # Re submit with new data.
        data['message_id'] = '<foo@bar.com>'
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(checkout.message_id, data['message_id'])

        # Re submit with new data, without the previous value.
        del data['message_id']
        data['valid'] = True
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertIsNotNone(checkout.message_id)
        self.assertEqual(checkout.valid, data['valid'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBCheckout.objects.filter(id=data['id']).count()
        )

    @responses.activate
    def test_patch(self):
        """Submit email patches."""
        body = (
            b'From foo@baz Mon 25 Nov 2019 02:27:19 PM CET\n'
            b'From: Some One <some-one@redhat.com>\n'
            b'Date: Tue, 19 Nov 2019 23:47:33 +0100\n'
            b'Subject: fix something somewhere\n'
            b'..')
        responses.add(responses.GET, 'http://some-patch.server/patch/1234', body=body)
        responses.add(responses.GET, 'http://some-patch.server/patch/1235', body=body)
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'patchset_files': [
                {'url': 'http://some-patch.server/patch/1234', 'name': '1234'},
                {'url': 'http://some-patch.server/patch/1235'}
            ],
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(2, checkout.patches.count())
        self.assertFalse(hasattr(checkout.patches.first(), 'patchworkpatch'))

        expected_keys = [
            # If "name" is provided, use it as a subject
            ('http://some-patch.server/patch/1234', '1234'),
            # Otherwise, use the subject from the patch body
            ('http://some-patch.server/patch/1235', 'fix something somewhere'),
        ]
        patch_keys = [patch.natural_key() for patch in checkout.patches.all()]
        self.assertEqual(expected_keys, patch_keys)

    def test_log_url(self):
        """Submit log_url."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'log_url': 'http://log.server/log.name',
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertIsInstance(checkout.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', checkout.log.url)
        self.assertEqual('log.name', checkout.log.name)

    def test_contacts(self):
        """Test contact submission."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>']
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual(2, checkout.contacts.count())
        contact_1 = checkout.contacts.get(email='someone@email.com')
        self.assertEqual('', contact_1.name)
        contact_2 = checkout.contacts.get(email='some-other@mail.com')
        self.assertEqual('Some Other', contact_2.name)

    def test_create_from_json_with_empty_strings(self):
        """Test object submission with empty strings on fields that get parsed into FKs."""
        data = {
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': '',
            'tree_name': '',
            'submitter': '',
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual('redhat:887318', str(checkout), 'Assert behavior of __str__()')

        self.assertIsNone(checkout.log)
        self.assertIsNone(checkout.tree)
        self.assertIsNone(checkout.submitter)

    @responses.activate
    def test_all_data(self):
        """Check it creates all the objects."""
        mock_patch()
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'tree_name': 'arm',
            'git_repository_url': 'https://repository.com/repo/kernel-rhel',
            'git_repository_branch': 'rhel-1.2.3',
            'git_commit_hash': '403cbf29a4e277ad4872515ec3854b175960bbdf',
            'git_commit_name': 'commit name',
            'patchset_files': [{'url': 'http://patchwork.server/patch/2322797/mbox/',
                                'name': 'mbox'}],
            'patchset_hash': '21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4',
            'message_id': '<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>',
            'comment': 'this is the comment',
            'start_time': '2020-06-01T06:47:41.108Z',
            'valid': True,
            'contacts': ['someone@email.com', 'Some Other <some-other@mail.com>'],
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'misc': {
                'brew_task_id': 123,
                'submitter': 'someone@email.com',
                'scratch': True,
                'is_public': True,
                'source_package_name': 'kernel',
                'all_sources_targeted': True,
                'patchset_modified_files': [
                    {'path': 'file/1'},
                    {'path': 'file/2'}
                ],
                'mr': {
                    'id': 9,
                    'url': 'https://url/to/merge/request',
                    'diff_url': 'https://url/to/merge/request',
                },
            },
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual('Some\nLog\nLines', checkout.log_excerpt)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields, except log_excerpt
        checkout = models.KCIDBCheckout.create_from_json({
            'id': data['id'],
            'origin': data['origin']
        })

        self.assertEqual('redhat:1', checkout.id)
        self.assertIsInstance(checkout.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', checkout.origin.name)

        self.assertIsInstance(checkout.tree, models.GitTree)
        self.assertEqual('arm', checkout.tree.name)

        self.assertEqual('https://repository.com/repo/kernel-rhel', checkout.git_repository_url)
        self.assertEqual('rhel-1.2.3', checkout.git_repository_branch)
        self.assertEqual('403cbf29a4e277ad4872515ec3854b175960bbdf', checkout.git_commit_hash)
        self.assertEqual('commit name', checkout.git_commit_name)
        self.assertEqual('<e41888bcd8ecf2e9bc8cc37c56386e01a5b43c56.some-one@redhat.com>', checkout.message_id)
        self.assertEqual('this is the comment', checkout.comment)
        self.assertEqual(True, checkout.valid)
        self.assertEqual('21c9a43d22cd02babb34b45a9defb881ae8228f0d034a0779b1321e851cad6a4', checkout.patchset_hash)
        self.assertEqual(None, checkout.log_excerpt)
        self.assertEqual(True, checkout.public)
        self.assertEqual(models.Policy.PUBLIC, checkout.policy.name)
        self.assertEqual(
            datetime.datetime(2020, 6, 1, 6, 47, 41, 108000, tzinfo=datetime.timezone.utc),
            checkout.start_time
        )

        self.assertEqual(123, checkout.brew_task_id)
        self.assertIsInstance(checkout.submitter, models.Maintainer)
        self.assertEqual('someone@email.com', checkout.submitter.email)
        self.assertEqual(True, checkout.scratch)
        self.assertEqual('kernel', checkout.source_package_name)
        self.assertEqual(True, checkout.all_sources_targeted)
        self.assertEqual([{'path': 'file/1'}, {'path': 'file/2'}], checkout.patchset_modified_files)
        self.assertEqual(
            {'id': 9, 'url': 'https://url/to/merge/request', 'diff_url': 'https://url/to/merge/request'},
            checkout.related_merge_request
        )
        self.assertFalse(checkout.is_test_plan)

    def test_report_rules(self):
        """Submit report rules."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'report_rules': '[{"when": "always", "send_to": "foo@bar.com"}]'
            }
        }

        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(
            [{'when': 'always', 'send_to': 'foo@bar.com'}],
            checkout.report_rules
        )

    @responses.activate
    def test_retrigger(self):
        """Check it sets the retrigger fields."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'retrigger': True,
            },
        }

        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual(models.Policy.RETRIGGER, checkout.policy.name)
        self.assertEqual(True, checkout.retrigger)

    def test_checkout_pointer(self):
        """Test checkout.checkout points to itself."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }
        checkout = models.KCIDBCheckout.create_from_json(data)
        self.assertEqual(checkout, checkout.checkout)

    def test_provenance(self):
        """Check provenance fields are correctly set."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor', 'misc': {'some': 'data'}},
                ]
            },
        }

        models.KCIDBCheckout.create_from_json(data)

        # Resubmit without provenance key, should not remove it
        del data['misc']['provenance']
        checkout = models.KCIDBCheckout.create_from_json(data)

        self.assertEqual(
            [(p.url, p.function, p.misc) for p in checkout.provenance.all()],
            [('http://1', 'c', None), ('http://2', 'e', {'some': 'data'})],
        )


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBBuildFromJson(utils.TestCase):
    """Test creation of KCIDBBuild model."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': 'redhat:1',
                'origin': 'redhat',
            }
        )

    def test_missing_parent(self):
        """Assert raises MissingParent as expected."""
        with self.assertRaises(models.MissingParent) as exception_ctx:
            data = {
                'checkout_id': 'nothing-like-this',
                'origin': 'redhat',
                'id': 'redhat:887318',
            }
            models.KCIDBBuild.create_from_json(data)
        self.assertEqual(str(exception_ctx.exception), 'KCIDBCheckout id=nothing-like-this is not present in the DB')

    @mock.patch('datawarehouse.metrics.update_time_to_build')
    def test_basic(self, mock_update_time_to_build):
        """Submit only id."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)

        self.assertEqual('redhat:887318', str(build), 'Assert behavior of __str__()')

        self.assertIsInstance(build, models.KCIDBBuild)
        self.assertIsInstance(build.checkout, models.KCIDBCheckout)
        self.assertEqual('redhat:1', build.checkout.id)
        self.assertEqual('redhat:887318', build.id)
        self.assertIsInstance(build.origin, models.KCIDBOrigin)
        self.assertEqual('redhat', build.origin.name)

        mock_update_time_to_build.delay.assert_called_with(build.iid)

    def test_re_submit(self):
        """Submit submit multiple times. Updates current data."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
        }
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNone(build.valid)

        # Re submit with new data.
        data['valid'] = False
        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(build.valid, data['valid'])

        # Re submit with new data, without the previous value.
        del data['valid']
        data['comment'] = 'foobar'
        build = models.KCIDBBuild.create_from_json(data)
        self.assertIsNotNone(build.valid)
        self.assertEqual(build.comment, data['comment'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBBuild.objects.filter(id=data['id']).count()
        )

    def test_submit_files(self):
        """Test files submission. log_url, input_files and output_files."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        build = models.KCIDBBuild.create_from_json(data)

        # log_url
        self.assertIsInstance(build.log, models.Artifact)
        self.assertEqual('http://log.server/log.name', build.log.url)
        self.assertEqual('log.name', build.log.name)

        # input_files
        self.assertListEqual(
            [{'url': 'http://log.server/input.file', 'name': 'input.file'},
             {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.input_files.order_by('url').all()]
        )

        # output_files
        self.assertListEqual(
            [{'url': 'http://log.server/output.file', 'name': 'output.file'},
             {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'}],
            [{'url': file.url, 'name': file.name} for file in build.output_files.order_by('url').all()]
        )

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'input_files': [
                {'url': 'http://log.server/input.file.1', 'name': 'input.file.1'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file.1', 'name': 'output.file.1'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }

        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(
            set(build.input_files.all()),
            set(models.Artifact.objects.filter(name__in=['input.file.1', 'input.file.2']))
        )
        self.assertEqual(
            set(build.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file.1', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'input_files': [
                {'url': 'http://log.server/input.file.3', 'name': 'input.file.3'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(
            set(build.input_files.all()),
            set(models.Artifact.objects.filter(name='input.file.3'))
        )
        self.assertEqual(
            set(build.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['input_files']
        del data['output_files']

        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual(build.input_files.count(), 1)
        self.assertEqual(build.output_files.count(), 1)

    def test_create_from_json_with_empty_strings(self):
        """Test object submission with empty strings on fields that get parsed into FKs."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'architecture': '',
            'compiler': '',
            'log_url': '',
        }

        build = models.KCIDBBuild.create_from_json(data)

        self.assertIsNone(build.architecture)
        self.assertIsNone(build.compiler)
        self.assertIsNone(build.log)

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'input_files': [
                {'url': 'http://log.server/input.file', 'name': 'input.file'},
                {'url': 'http://log.server/input.file.2', 'name': 'input.file.2'},
            ],
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            'start_time': '2020-06-03T15:14:57.215Z',
            'duration': 632,
            'architecture': 'aarch64',
            'command': 'make rpmbuild ...',
            'compiler': 'aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)',
            'config_name': 'fedora',
            'valid': True,
            'misc': {
                'test_plan_missing': True,
                'debug': True,
                'kpet_tree_name': 'kpet_tree_name',
                'package_name': 'package_name',
                'testing_skipped_reason': 'unsupported',
            }
        }

        build = models.KCIDBBuild.create_from_json(data)
        self.assertEqual('Some\nLog\nLines', build.log_excerpt)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields, except log_excerpt
        build = models.KCIDBBuild.create_from_json({
            'checkout_id': data['checkout_id'],
            'id': data['id'],
            'origin': data['origin']
        })

        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 14, 57, 215000, tzinfo=datetime.timezone.utc),
            build.start_time
        )
        self.assertEqual(632, build.duration)
        self.assertEqual('make rpmbuild ...', build.command)
        self.assertEqual('fedora', build.config_name)
        self.assertEqual(True, build.valid)

        self.assertEqual(models.ArchitectureEnum['aarch64'], build.architecture)
        self.assertIsInstance(build.compiler, models.Compiler)
        self.assertEqual('aarch64-linux-gnu-gcc (GCC) 8.2.1 20181105 (Red Hat Cross 8.2.1-1)', build.compiler.name)
        self.assertEqual(None, build.log_excerpt)
        self.assertIsInstance(build.log, models.Artifact)
        self.assertEqual(build.log.url, 'http://log.server/log.name')
        self.assertTrue(build.test_plan_missing)
        self.assertTrue(build.debug)

        self.assertEqual('kpet_tree_name', build.kpet_tree_name)
        self.assertEqual('package_name', build.package_name)
        self.assertEqual('unsupported', build.testing_skipped_reason)
        self.assertFalse(build.is_test_plan)

    def test_test_plan_missing(self):
        """Test misc/test_plan_missing flag submission and update."""
        cases = [
            # misc, expected value
            ({}, False),
            ({'test_plan_missing': True}, True),
            ({'test_plan_missing': False}, False),
        ]

        for misc, expected in cases:
            data = {
                'checkout_id': 'redhat:1',
                'origin': 'redhat',
                'id': 'redhat:887318',
                'misc': misc,
            }
            build = models.KCIDBBuild.create_from_json(data)
            self.assertEqual(expected, build.test_plan_missing)

    def test_retrigger(self):
        """Check it sets the retrigger fields."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:887318',
            'misc': {
                'retrigger': True
            }
        }

        build = models.KCIDBBuild.create_from_json(data)
        self.assertTrue(build.retrigger)

    def test_provenance(self):
        """Check provenance fields are correctly set."""
        data = {
            'checkout_id': 'redhat:1',
            'origin': 'redhat',
            'id': 'redhat:2',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor', 'misc': {'some': 'data'}},
                ]
            },
        }

        models.KCIDBBuild.create_from_json(data)

        # Resubmit without provenance key, should not remove it
        del data['misc']['provenance']
        build = models.KCIDBBuild.create_from_json(data)

        self.assertEqual(
            [(p.url, p.function, p.misc) for p in build.provenance.order_by('url').all()],
            [('http://1', 'c', None), ('http://2', 'e', {'some': 'data'})],
        )


@mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
class TestKCIDBTestFromJson(utils.TestCase):
    """Test creation of KCIDBTest model."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': 'redhat:1',
                'origin': 'redhat',
            }
        )
        models.KCIDBBuild.create_from_json(
            {
                'checkout_id': 'redhat:1',
                'origin': 'redhat',
                'id': 'redhat:887318',
                'architecture': 'aarch64',
            }
        )

    def test_missing_parent(self):
        """Assert raises MissingParent as expected."""
        with self.assertRaises(models.MissingParent) as exception_ctx:
            data = {
                'build_id': 'nothing-like-this',
                'id': 'redhat:111218982',
                'origin': 'redhat',
            }
            models.KCIDBTest.create_from_json(data)
        self.assertEqual(str(exception_ctx.exception), 'KCIDBBuild id=nothing-like-this is not present in the DB')

    def test_basic(self):
        """Submit only id."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertEqual('redhat:111218982', str(test), 'Assert behavior of __str__()')

        self.assertIsInstance(test, models.KCIDBTest)
        self.assertIsInstance(test.build, models.KCIDBBuild)
        self.assertEqual('redhat:887318', test.build.id)

        # These are False by default
        self.assertFalse(test.targeted)

    def test_re_submit(self):
        """Submit multiple times. Updates current data."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNone(test.waived)

        # Re submit with new data.
        data['waived'] = True
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.waived, data['waived'])

        # Re submit with new data, without the previous value.
        del data['waived']
        data['duration'] = 123
        test = models.KCIDBTest.create_from_json(data)
        self.assertIsNotNone(test.waived)
        self.assertEqual(test.duration, data['duration'])

        # Check there's only one instance
        self.assertEqual(
            1,
            models.KCIDBTest.objects.filter(id=data['id']).count()
        )

    def test_output_files(self):
        """Check created artifacts for output_files."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertListEqual(
            [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
            [
                {'url': file.url, 'name': file.name}
                for file in test.output_files.order_by('name').all()
            ]
        )

    def test_maintainers_empty(self):
        """Test maintainers field as comma separated string."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {'maintainers': None}
        }
        models.KCIDBTest.create_from_json(data)

    def test_maintainers_not_empty(self):
        """Test maintainers field as dict."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {
                'maintainers': [
                    {'name': 'Maintainer One', 'email': 'one@maintainer.com'},
                    {'email': 'two@maintainer.com', 'gitlab': 'two'},
                    {'email': 'three@maintainer.com'},
                ]
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        cases = [
            ('Maintainer One', 'one@maintainer.com', None),
            ('', 'two@maintainer.com', 'two'),
            ('', 'three@maintainer.com', None),
        ]

        for name, email, gitlab_username in cases:
            self.assertTrue(
                test.test.maintainers.filter(
                    name=name, email=email, gitlab_username=gitlab_username).exists(),
                (name, email)
            )

    def test_maintainers_overwrite(self):
        """Test behaviour with empty maintainers list. Should not overwrite."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
            'misc': {
                'maintainers': [
                    {'name': 'Maintainer One', 'email': 'one@maintainer.com'},
                ]
            }
        }
        models.KCIDBTest.create_from_json(data)

        # Resubmit test without maintainers
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'comment': 'Boot test',
        }
        kcidb_test = models.KCIDBTest.create_from_json(data)

        self.assertEqual(
            'Maintainer One',
            kcidb_test.test.maintainers.get().name
        )

    def test_create_from_json_with_empty_strings(self):
        """Test object submission with empty strings on fields that get parsed into FKs."""
        data = {
            'checkout_id': 'redhat:1',
            'build_id': 'redhat:887318',
            'origin': 'redhat',
            'id': 'redhat:111218982',
            'environment': '',
            'comment': '',
            'status': '',
            'log_url': '',
        }

        test = models.KCIDBTest.create_from_json(data)

        self.assertIsNone(test.environment)
        self.assertIsNone(test.test)
        self.assertIsNone(test.log)
        self.assertEqual(test.status, '')

    def test_all_data(self):
        """Test complete object submission."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'environment': {
                'comment': 'hostname.redhat.com'
            },
            'path': 'boot',
            'comment': 'Boot test',
            'waived': False,
            'start_time': '2020-06-03T15:52:25Z',
            'duration': 158,
            'status': 'PASS',
            'log_url': 'http://log.server/log.name',
            'log_excerpt': 'Some\nLog\nLines',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
            ],
            'misc': {
                'maintainers': [
                    {'name': 'Cosme Fulanito', 'email': 'cosme@fulanito.com'},
                    {'name': 'Pepe', 'email': 'pe@pe.com'}
                ],
                'targeted': True,
                'fetch_url': 'http://test.url',
                'polarion_id': '2111a0fa-1c27-4663-94d5-a951c509413e',
            }
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual('Some\nLog\nLines', test.log_excerpt)

        # Submit a base version without data to make sure resubmitting empty does not
        # override missing fields, except log_excerpt
        test = models.KCIDBTest.create_from_json({
            'build_id': data['build_id'],
            'id': data['id'],
            'origin': data['origin'],
        })

        self.assertIsInstance(test.environment, models.BeakerResource)
        self.assertEqual('hostname.redhat.com', test.environment.fqdn)

        self.assertEqual('P', test.status)

        self.assertIsInstance(test.test, models.Test)
        self.assertEqual('boot', test.test.universal_id)
        self.assertEqual('Boot test', test.test.name)
        self.assertEqual(2, test.test.maintainers.count())
        self.assertTrue(
            test.test.maintainers.filter(name='Cosme Fulanito', email='cosme@fulanito.com').exists()
        )
        self.assertTrue(
            test.test.maintainers.filter(name='Pepe', email='pe@pe.com').exists()
        )
        self.assertEqual('http://test.url', test.test.fetch_url)
        self.assertEqual('2111a0fa-1c27-4663-94d5-a951c509413e', test.polarion_id)

        self.assertEqual(False, test.waived)
        self.assertEqual(
            datetime.datetime(2020, 6, 3, 15, 52, 25, 0, tzinfo=datetime.timezone.utc),
            test.start_time
        )
        self.assertEqual(158, test.duration)

        self.assertEqual(1, test.output_files.count())
        self.assertTrue(test.targeted)

        self.assertEqual('http://log.server/log.name', test.log.url)
        self.assertEqual('log.name', test.log.name)
        self.assertEqual(None, test.log_excerpt)
        self.assertFalse(test.is_test_plan)

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(
            set(test.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(
            set(test.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['output_files']

        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.output_files.count(), 1)

    def test_retrigger(self):
        """Check it sets the retrigger fields."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'misc': {
                'retrigger': True,
            }
        }
        test = models.KCIDBTest.create_from_json(data)

        self.assertTrue(test.retrigger)

    def test_checkout_pointer(self):
        """Test test.checkout points to the checkout."""
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        test = models.KCIDBTest.create_from_json(data)
        self.assertEqual(test.build.checkout, test.checkout)

    def test_provenance(self):
        """Check provenance fields are correctly set."""
        data = {
            'build_id': 'redhat:887318',
            'origin': 'redhat',
            'id': 'redhat:111218982',
            'misc': {
                'provenance': [
                    {'url': 'http://1', 'function': 'coordinator'},
                    {'url': 'http://2', 'function': 'executor', 'misc': {'some': 'data'}},
                ]
            },
        }

        models.KCIDBTest.create_from_json(data)

        # Resubmit without provenance key, should not remove it
        del data['misc']['provenance']
        test = models.KCIDBTest.create_from_json(data)

        self.assertEqual(
            [(p.url, p.function, p.misc) for p in test.provenance.all()],
            [('http://1', 'c', None), ('http://2', 'e', {'some': 'data'})],
        )

    def test_results(self):
        """Assert TestResults are created and recreated as expected.

        KCIDBTestResult itself can be updated selecting by ID, but due to how
        those IDs are created by Beaker, it's unlikely they would be the same
        between runs. Therefore, the expected behavior is recreating them every time.
        """
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:1',
            'origin': 'redhat',
            'misc': {'results': []},
        }
        results_1_a = [{'id': 'redhat:1.1', 'status': 'FAIL'}, {'id': 'redhat:1.2', 'status': 'SKIP'}]
        expected_1_a = [('redhat:1.1', 'F'), ('redhat:1.2', 'S')]
        results_1_b = [{'id': 'redhat:1.1', 'status': 'FAIL'}, {'id': 'redhat:1.2', 'status': 'PASS'}]
        expected_1_b = [('redhat:1.1', 'F'), ('redhat:1.2', 'P')]
        results_2 = [{'id': 'redhat:1.3', 'status': 'ERROR'}, {'id': 'redhat:1.4', 'status': 'PASS'}]
        expected_2 = [('redhat:1.3', 'E'), ('redhat:1.4', 'P')]

        iids_before = []

        with self.subTest('Create TestResults'):
            data['misc']['results'] = results_1_a

            test = models.KCIDBTest.create_from_json(data)

            results = test.kcidbtestresult_set.all()
            self.assertQuerysetEqual(results.values_list('id', 'status'), expected_1_a)

            iids_after = list(results.values_list('iid', flat=True))
            self.assertNotEqual(iids_before, iids_after, "Expected KCIDBTestResult to be created")

            iids_before = iids_after

        with self.subTest('Update TestResults with the same IDs'):
            # NOTE: Updating a TestResult attribute is unexpected, but it's allowed.
            # The actual behavior we're seeking is a no-op when given the same KCIDB file.

            data['misc']['results'] = results_1_b

            test = models.KCIDBTest.create_from_json(data)

            results = test.kcidbtestresult_set.all()
            self.assertQuerysetEqual(results.values_list('id', 'status'), expected_1_b)

            iids_after = list(results.values_list('iid', flat=True))
            self.assertEqual(iids_before, iids_after, "Expected KCIDBTestResult to remain")

            iids_before = iids_after

        with self.subTest('Recreate TestResults with new IDs'):
            data['misc']['results'] = results_2

            test = models.KCIDBTest.create_from_json(data)

            results = test.kcidbtestresult_set.all()
            iids_after = list(results.values_list('iid', flat=True))

            self.assertQuerysetEqual(results.values_list('id', 'status'), expected_2)
            self.assertNotEqual(iids_before, iids_after, "Expected KCIDBTestResult to be recreated")

            iids_before = iids_after

        with self.subTest('Keep TestResults if missing key'):
            del data['misc']['results']

            test = models.KCIDBTest.create_from_json(data)

            results = test.kcidbtestresult_set.all()
            iids_after = list(results.values_list('iid', flat=True))

            self.assertQuerysetEqual(results.values_list('id', 'status'), expected_2)
            self.assertEqual(iids_before, iids_after, "Expected KCIDBTestResult to remain")

        with self.subTest('TestResults cleared if given empty list'):
            data['misc']['results'] = []

            test = models.KCIDBTest.create_from_json(data)
            results = test.kcidbtestresult_set.all()

            self.assertQuerysetEqual(results.values_list('id', 'status'), [],
                                     "Expected KCIDBTestResult to be removed")


class TestKCIDBNotifications(utils.TestCase):
    """Test kcidb notifications are sent only when the object is created or updated."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_new(self, signal):
        """Test send_kcidb_notification when the checkout is created."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_test_plan(self, signal):
        """Test send_kcidb_notification when the test plan is updated."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        # Update call
        models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_without_changes(self, signal):
        """Test send_kcidb_notification when the checkout is updated without changes."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'comment': 'comment',
            'valid': True,
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        # Update call
        models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_partially_without_changes(self, signal):
        """Test send_kcidb_notification when the checkout is updated partially without changes."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'comment': 'comment',
            'valid': True,
        }

        partial_data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'valid': True,
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        # Update call
        models.KCIDBCheckout.create_from_json(partial_data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_checkout_updated_with_changes(self, signal):
        """Test send_kcidb_notification when the checkout is updated with changes."""
        data = {
            'id': 'redhat:1',
            'origin': 'redhat',
            'comment': 'comment',
            'valid': False,
        }

        # Create call
        new = models.KCIDBCheckout.create_from_json(data)
        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='new',
                object_type='checkout',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

        signal.send.reset_mock()

        # Update call
        data['comment'] = 'Comment'
        updated = models.KCIDBCheckout.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.checkout.created_object_send_message',
                status='updated',
                object_type='checkout',
                objects=[updated]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_new(self, signal):
        """Test send_kcidb_notification when the build is created."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        }

        # Create call
        build = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[build]
            ),
        ])
        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_test_plan(self, signal):
        """Test send_kcidb_notification when the test plan is updated."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)
        # Update call
        models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_partially_without_changes(self, signal):
        """Test send_kcidb_notification when the build is partially updated without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'comment': 'comment',
            'valid': False,
        }

        partial_data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'valid': False,
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)
        # Update call
        models.KCIDBBuild.create_from_json(partial_data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_without_changes(self, signal):
        """Test send_kcidb_notification when the build is updated without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'valid': False,
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)
        # Update call
        models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_build_updated_with_changes(self, signal):
        """Test send_kcidb_notification when the build is updated with changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
            'valid': False,
        }

        # Create call
        new = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='new',
                object_type='build',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

        signal.send.reset_mock()
        # Update call
        data['comment'] = 'Comment'
        updated = models.KCIDBBuild.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.build.created_object_send_message',
                status='updated',
                object_type='build',
                objects=[updated]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_new(self, signal):
        """Test send_kcidb_notification when the test is created."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_test_plan(self, signal):
        """Test send_kcidb_notification when the test plan is updated."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)
        # Update call
        models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_without_changes(self, signal):
        """Test send_kcidb_notification when the test is updated without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'waived': True,
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)
        # Update call
        models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_partially_without_changes(self, signal):
        """Test send_kcidb_notification when the test is updated partially without changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'waived': True,
        }

        partial_data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)
        # Update call
        models.KCIDBTest.create_from_json(partial_data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_test_updated_with_changes(self, signal):
        """Test send_kcidb_notification when the test is updated with changes."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
            'waived': False,
        }

        # Create call
        new = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='new',
                object_type='test',
                objects=[new]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)

        signal.send.reset_mock()

        # Update call
        data['waived'] = True
        updated = models.KCIDBTest.create_from_json(data)

        signal.send.assert_has_calls([
            mock.call(
                sender='kcidb.test.created_object_send_message',
                status='updated',
                object_type='test',
                objects=[updated]
            ),
        ])

        self.assertEqual(1, signal.send.call_count)


class TestVisibility(utils.TestCase):
    """Test objects visibility."""

    def test_checkout(self):
        """Test checkout visibility."""
        checkout = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        # By default is private.
        self.assertFalse(checkout.is_public)

        checkout.public = False
        checkout.save()
        self.assertFalse(checkout.is_public)

        checkout.public = True
        checkout.save()
        self.assertTrue(checkout.is_public)

    def test_build(self):
        """Test build visibility."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build = models.KCIDBBuild.create_from_json({
            'checkout_id': rev.id,
            'origin': 'redhat',
            'id': 'redhat:887318',
        })

        # By default is private.
        self.assertFalse(build.is_public)

        build.checkout.public = False
        build.checkout.save()
        self.assertFalse(build.is_public)

        build.checkout.public = True
        build.checkout.save()
        self.assertTrue(build.is_public)

    def test_test(self):
        """Test test visibility."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        test = models.KCIDBTest.create_from_json({
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        })

        # By default is private.
        self.assertFalse(test.is_public)

        test.build.checkout.public = False
        test.build.checkout.save()
        self.assertFalse(test.is_public)

        test.build.checkout.public = True
        test.build.checkout.save()
        self.assertTrue(test.is_public)


class TestKCIDBProperties(utils.TestCase):
    """Test objects properties."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def test_checkout_web_url(self):
        """Test KCIDBCheckout's web_url property."""
        checkout = models.KCIDBCheckout.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/checkouts/{checkout.iid}', checkout.web_url)

    def test_build_web_url(self):
        """Test KCIDBBuild's web_url property."""
        build = models.KCIDBBuild.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/builds/{build.iid}', build.web_url)

    def test_test_web_url(self):
        """Test KCIDBTest's web_url property."""
        test = models.KCIDBTest.objects.first()
        with mock.patch('datawarehouse.models.kcidb_models.settings.DATAWAREHOUSE_URL', 'http://dw'):
            self.assertEqual(f'http://dw/kcidb/tests/{test.iid}', test.web_url)

    def test_checkout_is_missing_triage(self):
        """Test KCIDBCheckout's is_missing_triage property."""
        checkout = models.KCIDBCheckout.objects.first()

        # Valid, triage not needed.
        checkout.valid = True
        checkout.save()
        self.assertFalse(checkout.is_missing_triage)

        # Invalid, missing triage.
        checkout.valid = False
        checkout.save()
        self.assertTrue(checkout.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_checkout=checkout
        )
        self.assertFalse(checkout.is_missing_triage)

    def test_build_is_missing_triage(self):
        """Test KCIDBBuild's is_missing_triage property."""
        build = models.KCIDBBuild.objects.first()

        # Valid, triage not needed.
        build.valid = True
        build.save()
        self.assertFalse(build.is_missing_triage)

        # Invalid, missing triage.
        build.valid = False
        build.save()
        self.assertTrue(build.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_build=build
        )
        self.assertFalse(build.is_missing_triage)

    def test_test_is_missing_triage(self):
        """Test KCIDBTest's is_missing_triage property."""
        test = models.KCIDBTest.objects.first()

        # Passed, triage not needed.
        test.status = 'P'
        test.save()
        self.assertFalse(test.is_missing_triage)

        # Failed, missing triage.
        test.status = 'F'
        test.save()
        self.assertTrue(test.is_missing_triage)

        # Issue tagged, triage done.
        models.IssueOccurrence.objects.create(
            issue=models.Issue.objects.first(),
            kcidb_test=test
        )
        self.assertFalse(test.is_missing_triage)


class TestKCIDBObjectCompare(utils.TestCase):
    """Test the equivalence of KCIDB Objects."""

    def test_checkout(self):
        """Test checkout."""
        checkout1 = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        checkout2 = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df4',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        self.assertTrue(checkout1.is_equivalent_to(checkout1))
        self.assertFalse(checkout1.is_equivalent_to(checkout2))
        self.assertFalse(checkout1.is_equivalent_to('string'))

    def test_build(self):
        """Test build."""
        rev = models.KCIDBCheckout.objects.create(
            id='decd6167bf4f6bec1284006d0522381b44660df3',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build1 = models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        build2 = models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887319',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertTrue(build1.is_equivalent_to(build1))
        self.assertFalse(build1.is_equivalent_to(build2))
        self.assertFalse(build1.is_equivalent_to('string'))

    def test_test(self):
        """Test test."""
        rev = models.KCIDBCheckout.objects.create(
            id='redhat:1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=rev,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        data1 = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218983',
            'origin': 'redhat',
        }

        test1 = models.KCIDBTest.create_from_json(data)
        test2 = models.KCIDBTest.create_from_json(data1)

        self.assertTrue(test1.is_equivalent_to(test1))
        self.assertFalse(test1.is_equivalent_to(test2))
        self.assertFalse(test1.is_equivalent_to('string'))


class TestKCIDBTestResultFromJson(utils.TestCase):
    """Test creation of KCIDBTestResult model."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    @mock.patch('datawarehouse.signals.kcidb_object.send', mock.Mock())
    def setUp(self):
        """Set Up."""
        models.KCIDBCheckout.create_from_json(
            {
                'id': 'redhat:1',
                'origin': 'redhat',
            }
        )
        models.KCIDBBuild.create_from_json(
            {
                'checkout_id': 'redhat:1',
                'origin': 'redhat',
                'id': 'redhat:2',
                'architecture': 'aarch64',
            }
        )
        models.KCIDBTest.create_from_json(
            {
                'build_id': 'redhat:2',
                'origin': 'redhat',
                'id': 'redhat:3',
                'comment': 'redhat three',
            }
        )

    def test_basic(self):
        """Submit only id."""
        data = {
            'id': 'redhat:2.1',
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        result = models.KCIDBTestResult.create_from_json(test, data)

        self.assertIsInstance(result, models.KCIDBTestResult)
        self.assertEqual(result.test, test)
        self.assertEqual(result.id, 'redhat:2.1')

    def test_full(self):
        """Submit complete result."""
        data = {
            'id': 'redhat:2.1',
            'status': 'PASS',
            'name': 'sub result for test 2',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        result = models.KCIDBTestResult.create_from_json(test, data)

        self.assertIsInstance(result, models.KCIDBTestResult)
        self.assertEqual(result.test, test)
        self.assertEqual(result.id, 'redhat:2.1')
        self.assertEqual(result.status, models.ResultEnum.PASS)
        self.assertEqual(result.name, 'sub result for test 2')
        self.assertEqual(
            [{'url': file.url, 'name': file.name} for file in result.output_files.all()],
            data['output_files']
        )

    def test_checkout_pointer(self):
        """Test testresult.checkout points to the checkout."""
        data = {
            'id': 'redhat:2.1',
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        testresult = models.KCIDBTestResult.create_from_json(test, data)

        self.assertEqual(test.build.checkout, testresult.checkout)

    def test_re_submit_files(self):
        """Submit files multiple times."""
        data = {
            'id': 'redhat:2.1',
            'status': 'PASS',
            'name': 'sub result for test 2',
            'output_files': [
                {'url': 'http://log.server/output.file', 'name': 'output.file'},
                {'url': 'http://log.server/output.file.2', 'name': 'output.file.2'},
            ],
        }
        test = models.KCIDBTest.objects.get(id='redhat:3')
        result = models.KCIDBTestResult.create_from_json(test, data)

        self.assertEqual(
            set(result.output_files.all()),
            set(models.Artifact.objects.filter(name__in=['output.file', 'output.file.2']))
        )

        # With new files, the current ones are replaced
        data.update({
            'output_files': [
                {'url': 'http://log.server/output.file.3', 'name': 'output.file.3'},
            ],
        })

        result = models.KCIDBTestResult.create_from_json(test, data)
        self.assertEqual(
            set(result.output_files.all()),
            set(models.Artifact.objects.filter(name='output.file.3'))
        )

        # If no files are provided, the files are not removed
        del data['output_files']

        result = models.KCIDBTestResult.create_from_json(test, data)
        self.assertEqual(result.output_files.count(), 1)


class TestProvenanceComponentModel(utils.TestCase):
    """Test ProvenanceComponent objects."""

    def test_create_from_dict(self):
        """Check ProvenanceComponent objects created by create_from_json."""
        provenance = models.ProvenanceComponent.create_from_dict({
            'url': 'https://url',
            'function': 'coordinator',
            'service_name': 'gitlab',
            'misc': {'some': 'data'},
        })

        self.assertEqual(str(provenance), 'coordinator')
        self.assertEqual(provenance.url, 'https://url')
        self.assertEqual(provenance.function, models.ProvenanceComponentFunctionEnum.COORDINATOR)
        self.assertEqual(provenance.service_name, 'gitlab')
        self.assertEqual(provenance.misc, {'some': 'data'})

    def test_string_format(self):
        """Check ProvenanceComponent __str__ method."""
        test_cases = [
            ('https://gitlab.com/project/-/jobs/1234', 'gitlab', 'Gitlab Job'),
            ('https://gitlab.com/project/-/pipelines/1234', 'gitlab', 'Gitlab Pipeline'),
            ('https://beaker.foo.com/recipes/11508560', 'beaker', 'Beaker Recipe'),
            ('https://beaker.foo.com/not-recipes/1', 'beaker', 'coordinator'),
            ('https://foo.bar/foo', None, 'coordinator')
        ]
        for url, service_name, expected in test_cases:
            provenance = models.ProvenanceComponent.create_from_dict({
                'url': url, 'function': 'coordinator', 'service_name': service_name
            })

            self.assertEqual(str(provenance), expected)
