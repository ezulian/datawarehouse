"""Test custom QuerySets for KCIDB models."""
import json

from django.db.models.query import QuerySet
from django.utils import timezone

from datawarehouse import models
from datawarehouse.api.kcidb.serializers import DW_KCIDB_SCHEMA_VERSION
from tests import utils


class TestCheckoutAggregated(utils.TestCase):
    """Test aggregated data on a checkout."""

    fixtures = [
        'tests/fixtures/basic_policies.yaml',
    ]

    def setUp(self):
        data = {
            'version': DW_KCIDB_SCHEMA_VERSION,
            'checkouts': [
                {'origin': 'redhat', 'id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3', 'valid': False},
            ],
            'builds': [
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-1', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-2', 'valid': False},
                {'origin': 'redhat', 'checkout_id': 'redhat:decd6167bf4f6bec1284006d0522381b44660df3',
                 'id': 'redhat:build-3', 'valid': True},
            ],
            'tests': [
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-1', 'status': 'FAIL'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-2', 'status': 'ERROR'},
                {'origin': 'redhat', 'build_id': 'redhat:build-1', 'id': 'redhat:test-3', 'status': 'PASS'},
            ]
        }
        self.assert_authenticated_post(
            201, 'add_kcidbcheckout', '/api/1/kcidb/submit', json.dumps({'data': data}),
            content_type="application/json"
        )

    def _test_rev(self, cases, only_aggregated=False):
        """Test attrs on both rev and rev_aggregated."""
        rev_id = 'redhat:decd6167bf4f6bec1284006d0522381b44660df3'
        rev = models.KCIDBCheckout.objects.get(id=rev_id)
        rev_aggregated = models.KCIDBCheckout.objects.aggregated().get(id=rev_id)

        for attr, value in cases:
            if isinstance(value, QuerySet):
                check = self.assertQuerySetEqual
            else:
                check = self.assertEqual

            check(getattr(rev_aggregated, attr), value, attr)
            if not only_aggregated:
                check(getattr(rev, attr), value, attr)

    def test_nothing_triaged(self):
        """Test none objects were triaged."""
        rev = models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3')

        # Without aggregated call these methods are not available.
        self.assertFalse(hasattr(rev, 'stats_checkout_triaged'))
        self.assertFalse(hasattr(rev, 'stats_checkout_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_triaged'))
        self.assertFalse(hasattr(rev, 'stats_tests_untriaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_triaged'))
        self.assertFalse(hasattr(rev, 'stats_builds_untriaged'))
        self.assertIsNone(rev.has_objects_missing_triage)
        self.assertIsNone(rev.has_objects_with_issues)

        # No issues but failed jobs. All untriaged, no triaged.
        cases = [
            ('stats_checkout_untriaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_untriaged', True),
            ('stats_checkout_triaged', False),
            ('stats_builds_triaged', False),
            ('stats_tests_triaged', False),
            # No issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', False),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.none()),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
            # Checkout is not triaged.
            ('is_missing_triage', True),
        ]
        self._test_rev(cases)

    def test_partially_triaged(self):
        """Test some objects were triaged and some others not."""
        # Add some issues to some builds and tests.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)

        # We now have both triaged and untriaged jobs.
        cases = [
            ('stats_checkout_triaged', True),
            ('stats_checkout_untriaged', False),
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', True),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', True),
            # Some issues, missing triage.
            ('has_objects_missing_triage', True),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_triaged', models.KCIDBBuild.objects.filter(id='redhat:build-1')),
            ('builds_untriaged', models.KCIDBBuild.objects.filter(id='redhat:build-2')),
            # Checkout is triaged.
            ('is_missing_triage', False),
        ]
        self._test_rev(cases)

    def test_fully_triaged(self):
        """Test all objects were triaged."""
        # Add issue to all failures.
        issue = models.Issue.objects.create(
            kind=models.IssueKind.objects.create(description="fail 1", tag="1"),
            description='foo bar',
            ticket_url='http://some.url',
        )
        models.KCIDBCheckout.objects.get(id='redhat:decd6167bf4f6bec1284006d0522381b44660df3').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-1').issues.add(issue)
        models.KCIDBBuild.objects.get(id='redhat:build-2').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-1').issues.add(issue)
        models.KCIDBTest.objects.get(id='redhat:test-2').issues.add(issue)

        # We now have all issues triaged.
        cases = [
            ('stats_builds_triaged', True),
            ('stats_builds_untriaged', False),
            ('stats_tests_triaged', True),
            ('stats_tests_untriaged', False),
            # No failures missing triage.
            ('has_objects_missing_triage', False),
            ('has_objects_with_issues', True),
        ]
        self._test_rev(cases, only_aggregated=True)

        cases = [
            # Check list of triaged and untriaged jobs.
            ('builds_untriaged', models.KCIDBBuild.objects.none()),
            ('builds_triaged', models.KCIDBBuild.objects.filter(id__in=('redhat:build-1', 'redhat:build-2'))),
        ]
        self._test_rev(cases)


class TestKCIDBCheckoutRelatedManagers(utils.TestCase):
    """Test for KCIDBCheckout custom related managers"""

    fixtures = [
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_untriaged_tests_blocking(self):
        """Test untriaged_tests_blocking method."""
        checkout = models.KCIDBCheckout.objects.get(id='public_checkout')
        queryset = models.KCIDBTest.objects.filter(build__checkout=checkout)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            aggregated = models.KCIDBCheckout.objects.aggregated().get(id='public_checkout_valid')
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_blocking

            self.assertQuerysetEqual(empty, result)

        cases = [
            (models.ResultEnum.ERROR, None, empty),
            (models.ResultEnum.ERROR, False, empty),
            (models.ResultEnum.ERROR, True, empty),
            (models.ResultEnum.FAIL, None, queryset),
            (models.ResultEnum.FAIL, False, queryset),
            (models.ResultEnum.FAIL, True, empty),
        ]

        for status, waived, expected in cases:
            with self.subTest(status=status, waived=waived):
                queryset.update(status=status, waived=waived)
                self.assertQuerysetEqual(
                    expected,
                    checkout.untriaged_tests_blocking,
                )

    def test_untriaged_tests_non_blocking(self):
        """Test untriaged_tests_non_blocking method."""
        checkout = models.KCIDBCheckout.objects.get(id='public_checkout')
        queryset = models.KCIDBTest.objects.filter(build__checkout=checkout)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            aggregated = models.KCIDBCheckout.objects.aggregated().get(id='public_checkout_valid')
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_non_blocking

            self.assertQuerysetEqual(empty, result)

        cases = [
            (models.ResultEnum.ERROR, None, queryset),
            (models.ResultEnum.ERROR, False, queryset),
            (models.ResultEnum.ERROR, True, empty),
            (models.ResultEnum.FAIL, None, empty),
            (models.ResultEnum.FAIL, False, empty),
            (models.ResultEnum.FAIL, True, empty),
        ]

        for status, waived, expected in cases:
            with self.subTest(status=status, waived=waived):
                queryset.update(status=status, waived=waived)
                self.assertQuerysetEqual(
                    expected,
                    checkout.untriaged_tests_non_blocking,
                )


class TestCheckoutAnnotatedByArchitecture(utils.TestCase):
    """Test annotated_by_architecture data on a checkout."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/base_multiple_architectures.yaml',
    ]

    def test_counters(self):
        """Test annotated_by_architecture counters."""
        rev = (
            models.KCIDBCheckout.objects
            .annotated_by_architecture()
            .get(id='redhat:d1c47b385764aaa488bce182d944fa22bb1325d1')
        )

        test_cases = {
            'aarch64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 1,
                'tests_failed_untriaged': 1,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64': {
                'builds_ran': 1,
                'builds_failed': 0,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 0,
                'tests_ran': 1,
                'tests_failed': 0,
                'tests_failed_untriaged': 0,
                'tests_failed_waived': 0,
                'tests_with_issues': 0,
            },
            'ppc64le': {
                'builds_ran': 1,
                'builds_failed': 1,
                'builds_failed_untriaged': 0,
                'builds_with_issues': 1,
                'tests_ran': 3,
                'tests_failed': 1,
                'tests_failed_untriaged': 0,
                'tests_failed_waived': 1,
                'tests_with_issues': 2,
            },
        }

        for arch, fields in test_cases.items():
            for key, value in fields.items():
                self.assertEqual(getattr(rev, f'stats_{arch}_{key}_count'), value, (arch, key))

        # All architectures not included in test_case should be zero.
        for arch in models.ArchitectureEnum:
            if arch.name in test_cases:
                continue

            for key in test_cases['aarch64']:
                self.assertEqual(getattr(rev, f'stats_{arch.name}_{key}_count'), 0, (arch.name, key))

    def test_annotated_by_architeture(self):
        """Test annotated_by_architecture."""
        checkout = (
            models.KCIDBCheckout.objects
            .annotated_by_architecture()
            .get(id='redhat:d1c47b385764aaa488bce182d944fa22bb1325d1')
        )

        for arch in models.ArchitectureEnum:
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['builds']['ran'],
                getattr(checkout, f'stats_{arch.name}_builds_ran_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['builds']['failed'],
                getattr(checkout, f'stats_{arch.name}_builds_failed_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['ran'],
                getattr(checkout, f'stats_{arch.name}_tests_ran_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['failed'],
                getattr(checkout, f'stats_{arch.name}_tests_failed_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['tests']['failed_waived'],
                getattr(checkout, f'stats_{arch.name}_tests_failed_waived_count'))
            self.assertEqual(
                checkout.annotated_by_architecture[arch.name]['known_issues'],
                (
                    getattr(checkout, f'stats_{arch.name}_builds_with_issues_count') +
                    getattr(checkout, f'stats_{arch.name}_tests_with_issues_count')
                )
            )


class TestCheckoutFilterReadyToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_ready_to_report."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_ready_to_report(self):
        """All checkouts are tagged as ready_to_report."""
        models.KCIDBCheckout.objects.update(ready_to_report=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_checkouts_not_finished(self):
        """Checkouts have no valid value."""
        models.KCIDBCheckout.objects.update(valid=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_builds_not_finished(self):
        """Builds have no valid value."""
        models.KCIDBBuild.objects.update(valid=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_tests_not_finished(self):
        """Tests have no status value."""
        models.KCIDBTest.objects.update(status=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_not_triaged(self):
        """All objects finished but were not triaged."""
        models.KCIDBCheckout.objects.update(valid=True)
        models.KCIDBBuild.objects.update(valid=True)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_ready(self):
        """All objects finished and were triaged."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_ready_and_reported(self):
        """All objects finished, were triaged but already reported."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now(), ready_to_report=True)
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_test_plan_missing(self):
        """All objects finished, but a build still has test plan missing."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now(), test_plan_missing=True)
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        checkouts_ready = models.KCIDBCheckout.objects.exclude(
            kcidbbuild__test_plan_missing=True
        )
        self.assertNotEqual(0, checkouts_ready.count())

        self.assertQuerysetEqual(
            checkouts_ready,
            models.KCIDBCheckout.objects.filter_ready_to_report()
        )

    def test_failed_build(self):
        """There are builds with test_plan_missing=True but some builds failed."""
        models.KCIDBCheckout.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now(), test_plan_missing=True)
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        failed_build = models.KCIDBBuild.objects.first()

        # test_plan_missing=True and all builds succeeded, this checkout is not ready.
        self.assertFalse(failed_build.checkout in models.KCIDBCheckout.objects.filter_ready_to_report())

        failed_build.valid = False
        failed_build.save()

        # test_plan_missing=True but a build failed, this checkout is ready.
        self.assertTrue(failed_build.checkout in models.KCIDBCheckout.objects.filter_ready_to_report())


class TestCheckoutFilterBuildSetupsFinishedToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_build_setups_finished."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_notification_sent_build_steps_finished(self):
        """All checkouts are tagged as notification_sent_build_setups_finished."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_not_finished(self):
        """Builds without finished setup stage."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_finished_and_message_has_not_been_sent(self):
        """Build setups finished but the message has not been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.filter_has_builds(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )

    def test_build_setups_finished_and_message_has_been_sent(self):
        """Build setups finished and the message has been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=True)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_build_setups_finished()
        )


class TestCheckoutFilterTestsFinishedToReport(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_tests_finished."""

    fixtures = [
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_already_notification_sent_tests_finished(self):
        """All checkouts are tagged as notification_sent_tests_finished."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_not_finished(self):
        """Checkouts with tests unfinished."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=False)
        models.KCIDBTest.objects.update(status=None)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_and_message_has_not_been_sent(self):
        """Tests finished but the message has not been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=False)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.filter_has_tests(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_and_message_has_been_sent(self):
        """Tests finished and the message has been sent."""
        models.KCIDBCheckout.objects.update(notification_sent_tests_finished=True)
        models.KCIDBTest.objects.update(status='P')

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

    def test_tests_finished_test_plan_missing(self):
        """Tests finished when a build has test_plan_missing=True."""
        models.KCIDBTest.objects.update(status='P')

        self.assertTrue(
            models.KCIDBCheckout.objects.filter_has_tests().count() > 0
        )
        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.filter_has_tests(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )

        models.KCIDBBuild.objects.update(test_plan_missing=True)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_tests_finished()
        )


class TestKCIDBCheckoutQuerySetFilterHasBuilds(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_has_builds."""

    def test_checkouts_with_builds_and_without_builds(self):
        """Checkout with any builds."""
        checkout_with_build = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout_with_build,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_has_builds().count())
        self.assertEqual(checkout_with_build,
                         models.KCIDBCheckout.objects.filter_has_builds()[0]
                         )

    def test_all_checkouts_without_builds(self):
        """Neither checkouts have build."""
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_has_builds()
        )

    def test_all_checkouts_with_builds(self):
        """All checkouts have builds."""
        checkout = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_has_builds()
        )


class TestKCIDBCheckoutQuerySetFilterHasTests(utils.TestCase):
    """Unit tests for KCIDBCheckoutQuerySet.filter_has_tests."""

    def test_checkouts_with_tests_and_without_tests(self):
        """Checkout with any tests."""
        checkout_with_test = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout_with_test,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        models.KCIDBTest.create_from_json(data)

        self.assertEqual(1, models.KCIDBCheckout.objects.filter_has_builds().count())
        self.assertEqual(checkout_with_test,
                         models.KCIDBCheckout.objects.filter_has_builds()[0]
                         )

    def test_all_checkouts_without_tests(self):
        """Neither checkouts have tests."""
        models.KCIDBCheckout.objects.create(
            id='redhat-2',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.none(),
            models.KCIDBCheckout.objects.filter_has_tests()
        )

    def test_all_checkouts_with_tests(self):
        """All checkouts have tests."""
        checkout = models.KCIDBCheckout.objects.create(
            id='redhat-1',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        models.KCIDBBuild.objects.create(
            checkout=checkout,
            id='redhat:887318',
            origin=models.KCIDBOrigin.objects.get_or_create(name='redhat')[0],
        )
        data = {
            'build_id': 'redhat:887318',
            'id': 'redhat:111218982',
            'origin': 'redhat',
        }
        models.KCIDBTest.create_from_json(data)

        self.assertQuerysetEqual(
            models.KCIDBCheckout.objects.all(),
            models.KCIDBCheckout.objects.filter_has_tests()
        )


class TestKCIDBBuildRelatedManagers(utils.TestCase):
    """Test for KCIDBBuild custom related managers"""

    fixtures = [
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_untriaged_tests_blocking(self):
        """Test untriaged_tests_blocking method."""
        build = models.KCIDBBuild.objects.get(id='public_build_1')
        queryset = models.KCIDBTest.objects.filter(build=build)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            aggregated = models.KCIDBBuild.objects.aggregated().get(id='public_build_2')
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_blocking

            self.assertQuerysetEqual(empty, result)

        cases = [
            (models.ResultEnum.ERROR, None, empty),
            (models.ResultEnum.ERROR, False, empty),
            (models.ResultEnum.ERROR, True, empty),
            (models.ResultEnum.FAIL, None, queryset),
            (models.ResultEnum.FAIL, False, queryset),
            (models.ResultEnum.FAIL, True, empty),
        ]

        for status, waived, expected in cases:
            with self.subTest(status=status, waived=waived):
                queryset.update(status=status, waived=waived)
                self.assertQuerysetEqual(
                    expected,
                    build.untriaged_tests_blocking,
                )

    def test_untriaged_tests_non_blocking(self):
        """Test untriaged_tests_non_blocking method."""
        build = models.KCIDBBuild.objects.get(id='public_build_1')
        queryset = models.KCIDBTest.objects.filter(build=build)
        empty = models.KCIDBTest.objects.none()

        with self.subTest("aggregated() with stats_tests_untriaged=0 short-circuits"):
            aggregated = models.KCIDBBuild.objects.aggregated().get(id='public_build_2')
            with self.assertNumQueries(0):
                result = aggregated.untriaged_tests_non_blocking

            self.assertQuerysetEqual(empty, result)

        cases = [
            (models.ResultEnum.ERROR, None, queryset),
            (models.ResultEnum.ERROR, False, queryset),
            (models.ResultEnum.ERROR, True, empty),
            (models.ResultEnum.FAIL, None, empty),
            (models.ResultEnum.FAIL, False, empty),
            (models.ResultEnum.FAIL, True, empty),
        ]

        for status, waived, expected in cases:
            with self.subTest(status=status, waived=waived):
                queryset.update(status=status, waived=waived)
                self.assertQuerysetEqual(
                    expected,
                    build.untriaged_tests_non_blocking,
                )


class TestKCIDBTestQuerySet(utils.TestCase):
    """Test for KCIDBTestQuerySet"""

    fixtures = [
        'tests/fixtures/issues.yaml',
        'tests/kcidb/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
    ]

    def test_filter_untriaged_blocking(self):
        """Test filter_untriaged_blocking method."""
        queryset = models.KCIDBTest.objects.all()
        empty = models.KCIDBTest.objects.none()

        cases = [
            (models.ResultEnum.ERROR, None, empty),
            (models.ResultEnum.ERROR, False, empty),
            (models.ResultEnum.ERROR, True, empty),
            (models.ResultEnum.FAIL, None, queryset),
            (models.ResultEnum.FAIL, False, queryset),
            (models.ResultEnum.FAIL, True, empty),
        ]

        for status, waived, expected in cases:
            with self.subTest(status=status, waived=waived):
                queryset.update(status=status, waived=waived)
                self.assertQuerysetEqual(
                    expected,
                    models.KCIDBTest.objects.filter_untriaged_blocking(),
                )

    def test_filter_untriaged_non_blocking(self):
        """Test filter_untriaged_non_blocking method."""
        queryset = models.KCIDBTest.objects.all()
        empty = models.KCIDBTest.objects.none()

        cases = [
            (models.ResultEnum.ERROR, None, queryset),
            (models.ResultEnum.ERROR, False, queryset),
            (models.ResultEnum.ERROR, True, empty),
            (models.ResultEnum.FAIL, None, empty),
            (models.ResultEnum.FAIL, False, empty),
            (models.ResultEnum.FAIL, True, empty),
        ]

        for status, waived, expected in cases:
            with self.subTest(status=status, waived=waived):
                queryset.update(status=status, waived=waived)
                self.assertQuerysetEqual(
                    expected,
                    models.KCIDBTest.objects.filter_untriaged_non_blocking(),
                )
