"""Test the scripts.misc module."""
from unittest import mock

from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.api.kcidb import serializers
from datawarehouse.scripts import misc
from tests import utils


class ScriptsMiscTest(utils.TestCase):
    """Unit tests for the scripts.misc module."""
    fixtures = [
        'tests/fixtures/scripts_misc.yaml',
    ]

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch('datawarehouse.utils.MSG_QUEUE.bulk_add')
    def test_send_kcidb_object_for_retriage(self, bulk_add):
        """Test send_kcidb_object_for_retriage."""

        checkout_1 = models.KCIDBCheckout.objects.get(id='rev1')

        issue_regex_1 = models.IssueRegex.objects.get(pk=1)
        issue_regex_2 = models.IssueRegex.objects.get(pk=2)

        build_1 = models.KCIDBBuild.objects.get(id='redhat:build-1')

        test_1 = models.KCIDBTest.objects.get(id='redhat:test-1')
        test_2 = models.KCIDBTest.objects.get(id='redhat:test-2')

        misc.send_kcidb_object_for_retriage([
            {'issueregex_id': 999},  # Missing id
        ])
        self.assertFalse(bulk_add.called)

        misc.send_kcidb_object_for_retriage([
            {'issueregex_id': issue_regex_1.id},
            {'issueregex_id': issue_regex_2.id},
            {'issueregex_id': issue_regex_1.id},
            {'issueregex_id': 999},  # Missing id
        ])
        bulk_add.assert_has_calls([
            mock.call(models.ObjectStatusEnum.NEEDS_TRIAGE, 'checkout', [
                serializers.KCIDBCheckoutSerializer(checkout_1).data,
            ], {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]}),
            mock.call(models.ObjectStatusEnum.NEEDS_TRIAGE, 'build', [
                serializers.KCIDBBuildSerializer(build_1).data,
            ], {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]}),
            mock.call(models.ObjectStatusEnum.NEEDS_TRIAGE, 'test', [
                serializers.KCIDBTestSerializer(test_2).data,
                serializers.KCIDBTestSerializer(test_1).data,
            ], {'issueregex_ids': [issue_regex_1.id, issue_regex_2.id]}),
        ])
