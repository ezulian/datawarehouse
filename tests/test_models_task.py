"""Test the task_models module."""
import datetime
from unittest import mock

from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import models
from datawarehouse.models.task_models import LOGGER as task_logger
from tests import utils


class QueuedTaskTest(utils.TestCase):
    """Unit tests for the QueuedTask model."""

    @freeze_time("2010-01-02 09:00:00")
    def test_create_new(self):
        """Test creating a new QueuedTask."""
        task_1 = models.QueuedTask.create(
            name='do_foo',
            call_id='test_task_1',
            call_kwargs={'foo': 'bar'}
        )

        self.assertEqual('do_foo', task_1.name)
        self.assertEqual('test_task_1', task_1.call_id)
        self.assertEqual([{'foo': 'bar'}], task_1.calls_kwargs)
        self.assertEqual(datetime.datetime(2010, 1, 2, 9, 5, tzinfo=datetime.timezone.utc),
                         task_1.run_at)

        with self.subTest("Assert create() works as expected when given a list in call_kwargs."):
            task_2 = models.QueuedTask.create(
                name='do_bar',
                call_id='test_task_2',
                call_kwargs=[{'bar': 'foo'}]
            )

            self.assertEqual([{'bar': 'foo'}], task_2.calls_kwargs)

    def test_create_already_existing(self):
        """Test creating a QueuedTask when the task already exists."""
        with freeze_time("2010-01-02 09:00:00"):
            models.QueuedTask.create(
                name='do_foo',
                call_id='test_task',
                call_kwargs={'foo': 'bar'}
            )

        task = models.QueuedTask.objects.get()
        self.assertEqual(datetime.datetime(2010, 1, 2, 9, 5, tzinfo=datetime.timezone.utc),
                         task.run_at)

        with freeze_time("2010-01-02 09:02:00"):
            models.QueuedTask.create(
                name='do_foo',
                call_id='test_task',
                call_kwargs={'foo': 'baz'}
            )

        task.refresh_from_db()
        self.assertEqual([{'foo': 'bar'}, {'foo': 'baz'}], task.calls_kwargs)
        self.assertEqual(datetime.datetime(2010, 1, 2, 9, 7, tzinfo=datetime.timezone.utc),
                         task.run_at)

        with self.subTest("Assert create() works as expected when given a list in call_kwargs."):
            models.QueuedTask.create(
                name='do_foo',
                call_id='test_task',
                call_kwargs=[{'foo': 'sion'}]
            )

            task.refresh_from_db()
            self.assertEqual([{'foo': 'bar'}, {'foo': 'baz'}, {'foo': 'sion'}], task.calls_kwargs)

    def test_create_same_task_different_id(self):
        """Test creating QueuedTask with different ids."""
        self.assertFalse(models.QueuedTask.objects.exists())

        models.QueuedTask.create(name='task_1', call_id='task_1_1', call_kwargs=None)
        models.QueuedTask.create(name='task_1', call_id='task_1_1', call_kwargs=None)

        self.assertEqual(1, models.QueuedTask.objects.count())

        # Different name, same call_id
        models.QueuedTask.create(name='task_2', call_id='task_1_1', call_kwargs=None)
        # Same name, different call_id
        models.QueuedTask.create(name='task_1', call_id='task_1_2', call_kwargs=None)

        self.assertEqual(3, models.QueuedTask.objects.count())

    @mock.patch('datawarehouse.models.task_models.celery.app.signature', return_value=mock.Mock())
    def test_run(self, mock_celery):
        """Test running a QueuedTask."""
        with self.subTest("Assert runs successfully"):
            task = models.QueuedTask.create(
                name='task_1',
                call_id='task_1_1',
                call_kwargs={'foo': 'bar'},
                run_in_minutes=0,
            )
            task.run()

            mock_celery.assert_called_with('task_1')
            mock_celery.return_value.assert_called_with(task.calls_kwargs)

            self.assertFalse(models.QueuedTask.objects.filter(pk=task.pk).exists(),
                             "Expected task to have been deleted")

        with self.subTest("Assert retry workflow"):
            mock_celery.side_effect = Exception()

            task = models.QueuedTask.create(
                name='task_2',
                call_id='task_2_1',
                call_kwargs={'foo': 'bar'},
                run_in_minutes=0,
            )
            with self.assertLogs(logger=task_logger, level='ERROR') as log_ctx:
                task.run()

            expected_log = (
                f'ERROR:{task_logger.name}:'
                f'Error running QueuedTask ({task}), will be retried on a new task.'
            )
            self.assertTrue(log_ctx.output[0].startswith(expected_log))

            self.assertTrue(
                models.QueuedTask.objects.exclude(pk=task.pk).filter(name='task_2', call_id='task_2_1').exists(),
                "Expected a new task with the same identifier to have been created",
            )

        with self.subTest("Assert running twice logs an error"):
            task = models.QueuedTask.create(
                name='task_1',
                call_id='task_1_1',
                call_kwargs={'foo': 'bar'},
                run_in_minutes=0,
            )
            # Simulate running
            task.start_time = timezone.now()
            task.save()

            with self.assertLogs(logger=task_logger, level='ERROR') as log_ctx:
                task.run()

            expected_log = (
                f"ERROR:{task_logger.name}:"
                f"Tried to run QueuedTask ({task}), but it's not ready to run or already running"
            )
            self.assertIn(expected_log, log_ctx.output)


class QueuedTaskQuerySetTest(utils.TestCase):
    """Unit tests for the QueuedTaskQuerySet."""

    def test_filter_ready_to_run(self):
        """Test getting ready to run tasks."""
        def create_task(name, run_in):
            models.QueuedTask.objects.create(
                name=name,
                call_id=name,
                calls_kwargs=[],
                run_at=timezone.now() + datetime.timedelta(minutes=run_in)
            )

        tasks = [
            ('task_1', 1),
            ('task_2', 2),
            ('task_3', 3),
            ('task_4', 4),
            ('task_5', 5),
        ]

        with freeze_time("2010-01-02 09:00:00"):
            for name, run_in in tasks:
                create_task(name, run_in)

            self.assertEqual(
                [],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )

        with freeze_time("2010-01-02 09:02:00"):
            self.assertEqual(
                ['task_1', 'task_2'],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )

        with freeze_time("2010-01-02 09:04:00"):
            self.assertEqual(
                ['task_1', 'task_2', 'task_3', 'task_4'],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )

        with freeze_time("2010-01-02 09:10:00"):
            self.assertEqual(
                ['task_1', 'task_2', 'task_3', 'task_4', 'task_5'],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )

        with self.subTest("Assert filter_ready_to_run excludes tasks that already started."):
            models.QueuedTask.objects.filter(
                call_id__in=['task_1', 'task_2']
            ).update(start_time="2010-01-02 09:10:00+00:00")

            self.assertEqual(
                ['task_3', 'task_4', 'task_5'],
                [t.name for t in models.QueuedTask.objects.filter_ready_to_run()]
            )
