# Issue

To learn about issues, check the [Issues page].

## Get

Get a single Issue.

`GET /api/1/issue/$issue_id`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `issue_id` | `int` | Yes | ID of the Issue. |

Example of response:

```json
{
    "id": 1,
    "kind": {
        "id": 1,
        "description": "Kernel bug",
        "tag": "Kernel Bug"
    },
    "description": "Bug description.",
    "ticket_url": "https://bugzilla.redhat.com/show_bug.cgi?id=123456"
    "resolved": false,
},
```

## List

Get a list of Issues.

`GET /api/1/issue`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `resolved` | `bool` | No | Filter resolved or not resolved issues. |

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 1,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "Bug description.",
            "ticket_url": "https://bugzilla.redhat.com/show_bug.cgi?id=123456"
            "resolved": false,
        }
    ]
}
```

## Get IssueOccurrence for a KCIDB Object

Get a single IssueOccurrence related to a KCIDB object.

`GET /api/1/kcidb/$object/issues/occurrences/$issue_id`

<!-- markdownlint-disable line-length -->
| Name       | Type  | Required | Description                                        |
|------------|-------|----------|----------------------------------------------------|
| `object`   | `str` | Yes      | Name of the KCIDB object: checkout, build, test or testresult. |
| `issue_id` | `int` | Yes      | ID of the Issue.                                   |

Example of response:

```json
{
    "issue": {
        "id": 1075,
        "kind": {
            "id": 3,
            "description": "Unstable Test",
            "tag": "Unstable Test"
        },
        "description": "LTP - syscalls: waitid10.c:36: TFAIL: infop->si_status (0) != SIGFPE (8)",
        "ticket_url": "https://lists.linux.it/pipermail/ltp/2022-March/028110.html",
        "resolved": false,
        "resolved_at": null,
        "policy": {
            "id": 1,
            "name": "public"
        },
        "first_seen": "2022-03-16T08:55:51.749000Z"
    },
    "is_regression": false,
    "created_at": "2022-04-14T05:33:09.415318Z"
}
```

## List IssueOccurrences for a KCIDB Object

Get all IssueOccurrences related to a KCIDB object.

`GET /api/1/kcidb/$object/issues/occurrences`

| Name       | Type  | Required | Description                                        |
|------------|-------|----------|----------------------------------------------------|
| `object`   | `str` | Yes      | Name of the KCIDB object: checkout, build, test or testresult. |

Example of response:

```json
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "issue": {
                "id": 1075,
                "kind": {
                    "id": 3,
                    "description": "Unstable Test",
                    "tag": "Unstable Test"
                },
                "description": "LTP - syscalls: waitid10.c:36: TFAIL: infop->si_status (0) != SIGFPE (8)",
                "ticket_url": "https://lists.linux.it/pipermail/ltp/2022-March/028110.html",
                "resolved": false,
                "resolved_at": null,
                "policy": {
                    "id": 1,
                    "name": "public"
                },
                "first_seen": "2022-03-16T08:55:51.749000Z"
            },
            "is_regression": false,
            "created_at": "2022-04-14T05:33:09.415318Z"
        }
    ]
}
```
<!-- markdownlint-restore -->

## Create IssueOccurrences for a KCIDB Object

Link an Issue to a KCIDB object.

`POST /api/1/kcidb/$object/issues`

<!-- markdownlint-disable line-length -->
| Name            | Type  | Required | Description                                                               |
|-----------------|-------|----------|---------------------------------------------------------------------------|
| `object`        | `str` | Yes      | Name of the KCIDB object: checkout, build, test or testresult.           |
| `issue_id`      | `int` | Yes      | ID of the Issue.                                                          |

Example of response:

```json
{
    "id": 1,
    "kind": {
        "id": 1,
        "description": "Kernel bug",
        "tag": "Kernel Bug"
    },
    "description": "Bug description.",
    "ticket_url": "https://bugzilla.redhat.com/show_bug.cgi?id=123456"
    "resolved": false,
},
```
<!-- markdownlint-restore -->
[Issues Page]: ../issues.md
